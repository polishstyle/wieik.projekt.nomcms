package wieik.nom.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wieik.nom.dto.Theme;
import wieik.nom.exceptions.NOMException;

/**
 * Created by Sebastian on 2015-06-05.
 */
@Controller
@RequestMapping("/theme")
public class ThemeController extends BaseController {
    public static final String THEME_PATH = "theme";

    @RequestMapping(value = "/{css}", method = RequestMethod.GET)
    public String getSectionPage(@PathVariable String css, Model model) {
        Theme theme = themeService.getByName(css);
        String cssBody;
        if (theme == null) {
            try {
                theme = themeService.getDefault();
                cssBody = theme.getBody();
            } catch (NOMException e) {
                cssBody = "";
            }
        } else {
            cssBody = theme.getBody();
        }
        model.addAttribute("css", cssBody);
        return "cms/css";
    }
}
