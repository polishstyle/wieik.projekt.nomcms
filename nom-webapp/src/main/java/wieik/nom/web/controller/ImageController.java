package wieik.nom.web.controller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * Wrzucanie i pobieranie zdjec.
 */
@Controller
@RequestMapping(value = "/img")
public class ImageController extends BaseController {

    /*

    obrazki znajdowac sie bede w foldrze img w folderze tomcata (np d:/tomcat/img/a1.img)

    WRZUCANIE OBRAZKA
    <form method="POST" action="/NOM/img/upload" enctype="multipart/form-data">
    File to upload: <input type="file" name="file"><br />
    Name: <input type="text" name="name"><br /> <br />
    <input type="submit" value="Upload"> Press here to upload the file!
    </form>

    WYSWIETLANIE OBRAZKA
    <img src="/NOM/img/png/xxx">
    */


    private static final Logger logger = LoggerFactory
            .getLogger(ImageController.class);

    /**
     * Upload single file using Spring Controller
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public
    @ResponseBody
    String uploadFileHandler(@RequestParam("name") String name,
                             @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "img");
                if (!dir.exists())
                    dir.mkdirs();


                byte[] bytes = file.getBytes();
                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath()
                        + File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                logger.info("Server File Location="
                        + serverFile.getAbsolutePath());

                return "You successfully uploaded file = " + name;
            } catch (Exception e) {
                e.printStackTrace();
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name
                    + " because the file was empty.";
        }
    }

    @RequestMapping(value = "/png/{name}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable(value = "name") String name) throws IOException {
        try {
            String rootPath = System.getProperty("catalina.home");
            File dir = new File(rootPath + File.separator + "img");
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir.getAbsolutePath() + File.separator + name + ".png");
            InputStream in = new BufferedInputStream(new FileInputStream(file));

            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);

            return new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
