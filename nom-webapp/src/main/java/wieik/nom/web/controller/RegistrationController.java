package wieik.nom.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.nom.dto.Account;
import wieik.nom.exceptions.NOMException;
import wieik.nom.web.form.AccountActivationForm;
import wieik.nom.web.form.EmailForm;
import wieik.nom.web.form.UserAccountForm;

import javax.validation.Valid;

/**
 * Created by Sebastian on 2014-11-13.
 */
@Controller
@RequestMapping("/rejestracja")
public class RegistrationController extends BaseController {
    private static final String REGISTRATION_FORM = "account/userRegistrationForm";
    private static final String REGISTRATION_OK = "redirect:/";
    private static final String ACTIVATION_FORM = "account/accountActivationForm";
    private static final String ACTIVATION_OK = "account/accountActivationPage";
    private static final String REACTIVATION_FORM = "account/accountReActivationForm";

    // ktore pola mozna wypelniac
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields(new String[]{
                "username", "password", "confirmPassword", "email", "sex"
        });
    }

    @RequestMapping(value = "/nowe", method = RequestMethod.GET)
    public String getRegistrationForm(Model model) {
        model.addAttribute("userAccountForm", new UserAccountForm());
        return REGISTRATION_FORM;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postRegistrationForm(@Valid UserAccountForm userAccountForm, BindingResult result) {
        try {
            convertPasswordError(result);
            String password = userAccountForm.getPassword();
            String answer = userAccountForm.getAnswer();
            accountService.registerUserAccount(toAccount(userAccountForm), password, answer, result);
            return (result.hasErrors() ? REGISTRATION_FORM : REGISTRATION_OK);
        } catch (NOMException e) {

        }
        return "mainPage";
    }

    @RequestMapping(value = "/aktywacja", method = RequestMethod.GET)
    public String getAccountActivationForm(Model model) {
        AccountActivationForm form = new AccountActivationForm();
        model.addAttribute("accountActivationForm", form);
        return ACTIVATION_FORM;
    }

    @RequestMapping(value = "/ponow_aktywacje", method = RequestMethod.GET)
    public String getSendAccountActivationForm(Model model) {
        EmailForm form = new EmailForm();
        model.addAttribute("emailForm", form);
        return REACTIVATION_FORM;
    }

    @RequestMapping(value = "/ponow_aktywacje", method = RequestMethod.POST)
    public String postSendAccountActivationForm(Model model, @Valid EmailForm emailForm, BindingResult result) {
        try {
            accountService.sendActivationEmail(emailForm.getEmail());
            model.addAttribute("sent", true);
        } catch (NOMException e) {
            model.addAttribute("failed", true);
        }
        return REACTIVATION_FORM;
    }

    @RequestMapping(value = "/aktywacja", method = RequestMethod.POST)
    public String postActivationForm(Model model, @Valid AccountActivationForm accountActivationForm, BindingResult result) {
        try {
            accountService.activateAccount(accountActivationForm.getEmail(), accountActivationForm.getActivationCode());
            return ACTIVATION_OK;
        } catch (NOMException e) {
            model.addAttribute("failed", true);
        }
        return ACTIVATION_FORM;
    }

    @RequestMapping(value = "/aktywuj", method = RequestMethod.GET)
    public String accountActivation(@RequestParam("a") long id, @RequestParam("d") String d, Model model) {
        try {
            accountService.activateAccount(id, d);
            return ACTIVATION_OK;
        } catch (NOMException e) {
            model.addAttribute("failed", true);
        }
        AccountActivationForm form = new AccountActivationForm();
        model.addAttribute("accountActivationForm", form);
        return ACTIVATION_FORM;
    }

    private static Account toAccount(UserAccountForm form) {
        Account account = new Account();
        account.setUsername(form.getUsername());
        account.setEmail(form.getEmail());
        account.setSex(form.getSex());
        account.setQuestion(form.getQuestion());
        return account;
    }

    private static void convertPasswordError(BindingResult result) {
        for (ObjectError error : result.getGlobalErrors()) {
            String msg = error.getDefaultMessage();
            if ("account.password.mismatch.message".equals(msg)) {
                if (!result.hasFieldErrors("password")) {
                    result.rejectValue("password", "error.mismatch");
                }
            }
        }
    }


}
