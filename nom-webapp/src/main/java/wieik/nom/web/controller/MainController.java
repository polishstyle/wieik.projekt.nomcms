package wieik.nom.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import wieik.nom.dao.AccountDao;
import wieik.nom.dao.ArticleDao;
import wieik.nom.dao.CommentDao;
import wieik.nom.dto.*;
import wieik.nom.exceptions.NOMException;
import wieik.nom.vo.LinkVO;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/*
    Kontroler odpowiadajacy za wyswietlanie tresci na stronach.
 */
@Controller
@RequestMapping("/")
public class MainController extends BaseController {
    private final static int LINKS_ON_SECTION_PAGE = 20;
    private final static int TOP_LINKS_ON_SECTION_PAGE = 5;
    private final static int TOP_LINKS_ON_MAIN_PAGE = 5;
    private final static int NEWEST_LINKS_ON_SECTION_PAGE = 10;
    private final static int LINKS_FOR_SECTION_ON_MAIN_PAGE = 10;
    private final static String DEFAULT_THEME_NAME = "standard";
    private final static String ARTICLE_FORM_PAGE = "newArticlePage";
    private final static String USER_PROFILE = "/account/userProfile";

    private CommentDao commentDao;

    @Autowired(required=true)
    @Qualifier(value="commentDao")
    public void setCommentDao(CommentDao commentDao){ this.commentDao = commentDao;
    }

    private AccountDao accountDao;

    @Autowired(required=true)
    @Qualifier(value="accountDao")
    public void setAccountDao(AccountDao accountDao){
        this.accountDao = accountDao;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(Model model) {
        model.addAllAttributes(mainPageAttributes());
        return "mainPage";
    }

    private Map<String, ?> mainPageAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("pageTitle", "NOM-Info");
        attributes.put("themeCssUrl", themePath(DEFAULT_THEME_NAME));
        attributes.put("style", themeService.getByName(DEFAULT_THEME_NAME).getBody());
        attributes.put("topLinks", articleService.getTopLinks(TOP_LINKS_ON_MAIN_PAGE));
        attributes.put("sectionUrl", "/NOM/p/");
        attributes.put("topLinksForSections", sectionService.getSectionVOsWithTopLinks(TOP_LINKS_ON_MAIN_PAGE));
        attributes.put("sections", sectionService.getSectionVOs(LINKS_FOR_SECTION_ON_MAIN_PAGE));
        attributes.put("editorMode", false);
        return attributes;
    }

    @RequestMapping(value = "/userProfile", params = {"userName"}, method = RequestMethod.GET)
    public String getUserProfile(@RequestParam String userName, Model model) {
        UserProfile userProfile = accountDao.getUserProfile(userName);

        model.addAttribute("userName", userName);

        model.addAttribute("Name", userProfile.getName());
        model.addAttribute("Surname", userProfile.getSurname());
        model.addAttribute("Age", userProfile.getAge());
        model.addAttribute("Hobby", userProfile.getHobby());

        return USER_PROFILE;
    }

    @RequestMapping(value = "/p/{section}", method = RequestMethod.GET)
    public String getSectionPage(@PathVariable(value = "section") String sectionName,
                                 Model model) {
        Section section = getSection(sectionName);
        if (section != null) {
            model.addAllAttributes(sectionAttributes(section, 1));
            return "mainTemplate";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/p/{section}/s/{page}", method = RequestMethod.GET)
    public String getSectionPage(@PathVariable(value = "section") String sectionName,
                                 @PathVariable(value = "page") int page,
                                 Model model) {
        Section section = getSection(sectionName);
        if (section != null) {
            model.addAllAttributes(sectionAttributes(section, page));
            return "mainTemplate";
        } else {
            return "redirect:/";
        }
    }

    private Map<String, ?> sectionAttributes(Section section, int currentPage) {
        Map<String, Object> sectionAttributes = new HashMap<String, Object>();
        sectionAttributes.put("pageTitle", section.getName());
        sectionAttributes.put("themeCssUrl", themePath(section.getDefaultTheme().getName()));
        sectionAttributes.put("contentViewName", "sectionPage.jsp");
        sectionAttributes.put("newestLinks", newestLinks(section));
        sectionAttributes.put("sectionName", section.getName());
        sectionAttributes.put("topLinks", sectionService.getLinksWithPriority(section, Article.PRIORITY_MAX, TOP_LINKS_ON_SECTION_PAGE));
        sectionAttributes.put("sectionLinks", sectionService.getLinks(section, currentPage, LINKS_ON_SECTION_PAGE));
        sectionAttributes.put("prevPage", currentPage > 1);
        sectionAttributes.put("nextPage", currentPage < getNumberOfPages(sectionService.coutArticles(section), LINKS_ON_SECTION_PAGE));
        sectionAttributes.put("currentPage", currentPage);
        sectionAttributes.put("sectionPageUrl", "/NOM/p/" + section.getName() + "/s/");
        sectionAttributes.put("style", themeService.getByName(section.getDefaultTheme().getName()).getBody());
        sectionAttributes.put("sectionsLink", "/NOM/p/");
        sectionAttributes.put("sectionNames", sectionNames());
        sectionAttributes.put("editorMode", false);
        return sectionAttributes;
    }

    private String themePath(String themeName) {
        return "/" + ThemeController.THEME_PATH + "/" + themeName + ".css";
        //return "/resources/css/" + themeName + ".css";
    }

    private List<LinkVO> newestLinks(Section section) {
        int page = 1;
        return sectionService.getLinks(section, page, NEWEST_LINKS_ON_SECTION_PAGE);
    }

    @RequestMapping(value = "/p/{section}/{article}", method = RequestMethod.GET)
    public String getArticlePage(@PathVariable(value = "section") String sectionName,
                                 @PathVariable(value = "article") String articleName,
                                 Model model) {
        Section section = getSection(sectionName);
        if (section != null) {
            Article article = articleService.findArticle(articleName);
            //section.setName(sectionName);
            //article.setSection(section);
            if (article != null) {
                model.addAllAttributes(articleAttributes(section, article, 1));
                return "mainTemplate";
            } else {
                return "redirect:/p/" + sectionName;
            }
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/p/{section}/{article}", params = {"deleteComment","commentContent"}, method = RequestMethod.GET)
    public String getDeleteComment(@PathVariable(value = "section") String sectionName,
                                 @PathVariable(value = "article") String articleName,
                                 @RequestParam String commentContent,
                                 Model model) {
        commentDao.deleteComment(commentContent);

        return "redirect:/p/" + sectionName;
    }

    //TODO: szukanie wulgaryzmow
    @RequestMapping(value = "/p/{section}/{article}/dodaj_komentarz", method = RequestMethod.POST)
    public String addCommentAndGetArticlePage(@PathVariable(value = "section") String sectionName,
                                              @PathVariable(value = "article") String articleName,
                                              @RequestParam("comment") String comment,
                                              Model model) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (isUser(authentication)) {
                articleService.addComment(articleName, comment, authentication.getName());
            }
        } catch (Exception e) {
        }
        return "redirect:/p/" + sectionName + '/' + articleName;
    }

    @RequestMapping(value = "/p/{section}/{article}/c/{commentPage}", method = RequestMethod.GET)
    public String getArticlePage(@PathVariable(value = "section") String sectionName,
                                 @PathVariable(value = "article") String articleName,
                                 @PathVariable(value = "commentPage") int commentPage,
                                 Model model) {
        Section section = getSection(sectionName);
        if (section != null) {
            Article article = articleService.findArticle(articleName);
            if (article != null) {
                model.addAllAttributes(articleAttributes(section, article, commentPage));
                return "cms/mainTemplate";
            } else {
                return "redirect:/p/" + sectionName;
            }
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/p/{section}/{article}/{vote}", method = RequestMethod.GET)
    public String voteAndGetArticlePage(@PathVariable(value = "section") String sectionName,
                                        @PathVariable(value = "article") String articleName,
                                        @PathVariable(value = "vote") int vote,
                                        Model model) {
        if (vote == 0 || vote == 1) {
            try {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (isUser(authentication)) {
                    articleService.tryToVote(articleName, vote, authentication.getName());
                }
            } catch (Exception e) {
            }
        }
        return "redirect:/p/" + sectionName + '/' + articleName;
    }

    private Map<String, ?> articleAttributes(Section section, Article article, int commentPage) {
        Map<String, Object> articleAttributes = new HashMap<String, Object>();
        articleAttributes.put("pageTitle", article.getTitle());
        articleAttributes.put("themeCssUrl", themePath(article.getTheme().getName()));
        articleAttributes.put("style", themeService.getByName(article.getTheme().getName()).getBody());
        articleAttributes.put("contentViewName", "articlePage.jsp");
        articleAttributes.put("article", article);
        articleAttributes.put("newestLinks", newestLinks(section));
        articleAttributes.put("voteUrl", "/NOM/p/" + section.getName() + '/' + article.getName());
        articleAttributes.put("comments", articleService.getComments(article, commentPage, 10));
        articleAttributes.put("currentPage", commentPage);
        articleAttributes.put("commentPageUrl", "/NOM/p/" + section.getName() + '/' + article.getName() + "/c/");
        articleAttributes.put("addCommentUrl", "/NOM/p/" + section.getName() + '/' + article.getName() + "/dodaj_komentarz/");
        articleAttributes.put("pages", articleService.commentsPages(article, 10));
        articleAttributes.put("sectionsLink", "/NOM/p/");
        articleAttributes.put("sectionNames", sectionNames());
        articleAttributes.put("editorMode", isEditorMode(section, article));
        articleAttributes.put("sectionName", section.getName());
        articleAttributes.put("themeName", article.getTheme().getName());
        return articleAttributes;
    }

    private boolean isEditorMode(Section section, Article article) {
        Account account = null;
        Editor editor = null;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication==null || !isEditor(authentication)) {
                System.err.println("!!! 1");
                return false;
            }
            account = accountService.findByUsername(authentication.getName());
            if (account==null) {
                System.err.println("!!! 2");
                return false;
            }
            editor = editorService.getEditor(account);
            if(editor==null) {
                System.err.println("!!! 3");
                return false;
            }
        } catch (Exception e) {
            System.err.println("!!! 4");
            return false;
        }
        if (!articleService.canEdit(article, editor)) {
            System.err.println("!!! 5");
            return false;
        }
        System.err.println("!!! 6");
        return true;
    }

    private List<String> sectionNames() {
        return sectionService.getSectionNames();
    }


    private Section getSection(String sectionName) {
        Section section = null;
        try {
            section = sectionService.getSectionByName(sectionName);
        } catch (NOMException e) {

        }
        return section;
    }

    private int getNumberOfPages(int total, int max) {
        int pages = 0;
        if (total > max)
            pages = (total / max);
        if ((total % max) > 0)
            ++pages;
        return pages;
    }

    @RequestMapping(value = "/art", method = RequestMethod.GET)
    @PreAuthorize("isAnonymous()")
    public String getNewArticlePage() {

        return "mainTemplate";
    }


    /*
     * DODAWANIE I EDYCJA ARTYKULU
     */
    //fixme: ma byc czysty kod
    @RequestMapping(value = "/art", method = RequestMethod.POST)
    public String postArticleForm(Model model,
                                  @RequestParam("sectionName") String sectionName,
                                  @RequestParam("oldName") String oldName,
                                  @RequestParam("name") String name,
                                  @RequestParam("title") String title,
                                  @RequestParam("preview") String preview,
                                  @RequestParam("themeName") String themeName,
                                  @RequestParam("content") String content,
                                  @RequestParam("image") MultipartFile image) {

        // CZY WYSYLAJACY ZADANIE MA ODPOWIEDNIE PRAWA
        //fixme: poprawic sprawdzanie
        Account account = null;
        Editor editor = null;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication==null || !isEditor(authentication)) {
                return "redirect:";
            }
            account = accountService.findByUsername(authentication.getName());
            if (account==null)
                return "redirect:";
            editor = editorService.getEditor(account);
            if(editor==null)
                return "redirect:";
        } catch (Exception e) {
            return "redirect:";
        }


        // WALIDACJA
        //fixme: przerobic na normalna walidacje
        String valid = "";
        Pattern sectionNamePattern = Pattern.compile(Section.NAME_REGEXP);
        if (!sectionNamePattern.matcher(sectionName).matches()) {
            valid = "Nie ma takiej sekcji.\n";
        }

        if (!sectionNamePattern.matcher(name).matches()) {
            valid.concat("Zly link do artykulu.\n");
        }

        if (title == null || title.length() < Article.TITLE_MIN_SIZE || title.length() > Article.TITLE_MAX_SIZE) {
            valid.concat("Zly tytul do artykulu: od " + Article.TITLE_MIN_SIZE + " do " + Article.TITLE_MAX_SIZE + " znak�w.\n");
        }

        if (themeName==null) {
            valid.concat("Zla nazwa motywu.\n");
        }

        if (content == null || content.isEmpty()) {
            valid.concat("Brak tresci.\n");
        }

        Section section = null;
        Article article = null;
        Theme theme = null;
        try {
            section = sectionService.getSectionByName(sectionName);
            theme = themeService.getByName(themeName);
            if (section==null) {
                valid.concat("Nie ma takiej sekcji.");
            }
            if (theme==null) {
                valid.concat("Nie ma takiego motywu.");
            }
            article = articleService.findArticle(oldName);



            // CZY WYSYLAJACY ZADANIE MA PRAWO DO SEKCJI/ARTYKULU
            if (article == null) {
                // czy moze tworzyc w sekcji
                if (!sectionService.canCreate(section, editor)) {
                    valid.concat("Nie masz praw do tworzenia artykulow w tej sekcji.\n");
                }
            } else {
                // czy moze edytowac artykul
                if (!isEditorMode(section, article)) {
                    valid.concat("Nie masz praw do edycji tego artykulu.\n");
                }
            }

            // OBSLUGA OBRAZKA
            String imageName = null;

            if (image!=null && !image.isEmpty()) {
                try {
                    imageName = name + System.currentTimeMillis() + ".png";

                    String rootPath = System.getProperty("catalina.home");
                    File dir = new File(rootPath + File.separator + "img");
                    if (!dir.exists())
                        dir.mkdirs();


                    byte[] bytes = image.getBytes();
                    // Create the file on server
                    File serverFile = new File(dir.getAbsolutePath()
                            + File.separator + imageName);
                    BufferedOutputStream stream = new BufferedOutputStream(
                            new FileOutputStream(serverFile));
                    stream.write(bytes);
                    stream.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    valid.concat("Wgranie obrazka nie powiodlo sie.\n");
                    imageName = null;
                }
            }


            if (valid.isEmpty()) {
                // OBSLUGA
                // czy artykul istnieje czy jest nowy
                if (article == null) {
                    article = new Article(title, name, content, preview, imageName, section, editor, theme);
                    articleService.create(article);
                } else {
                    article.setContent(content);
                    article.setName(name);
                    article.setTitle(title);
                    article.setTheme(theme);
                    if (imageName!=null) {
                        article.setImageName(imageName);
                    }
                    articleService.update(article, account.getUsername());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            valid = "BLAD: " + e.toString();
        }

        if (!valid.isEmpty()) {
            model.addAttribute("sectionName",sectionName);
            model.addAttribute("name",name);
            model.addAttribute("title",title);
            model.addAttribute("themeName",themeName);
            model.addAttribute("content",content);
            model.addAttribute("preview",preview);
            model.addAttribute("error", valid);
            model.addAttribute("contentViewName", ARTICLE_FORM_PAGE);
            return "mainTemplate";
        }

        return "redirect:";
    }

    @RequestMapping(value = "/nowy", method = RequestMethod.GET)
    public String getNewArticlePage(Model model) {
        Account account = null;
        Editor editor = null;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication==null || !isEditor(authentication)) {
                return "redirect:";
            }
        } catch (Exception e) {
            return "redirect:";
        }

        model.addAttribute("pageTitle", "Nowy artykul");
        model.addAttribute("themeCssUrl", themePath("standard"));
        model.addAttribute("style", themeService.getByName("standard").getBody());
        model.addAttribute("sectionName", "");
        model.addAttribute("name","");
        model.addAttribute("title","");
        model.addAttribute("themeName","");
        model.addAttribute("content","");
        model.addAttribute("preview","");
        model.addAttribute("error", "");
        return ARTICLE_FORM_PAGE;
    }

    protected boolean isEditor(Authentication authentication) throws NOMException {
        boolean authenticated = false;
        for (GrantedAuthority a : authentication.getAuthorities()) {
            if (a.getAuthority().equals("EDITOR")) {
                authenticated = true;
            }
        }
        return authenticated;
    }
}