package wieik.nom.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wieik.nom.dto.Account;
import wieik.nom.exceptions.NOMException;
import wieik.nom.web.form.AnswerQuestionForm;
import wieik.nom.web.form.ChangePasswordForm;
import wieik.nom.web.form.EmailForm;

import javax.validation.Valid;

/**
 * Created by Sebastian on 2015-03-22.
 */
@Controller
@RequestMapping("/konto")
public class AccountController extends BaseController {
    private static final String ACCOUNT_MANAGMENT_PAGE = "account/accountManagment";
    private static final String RESET_PASSWORD_FORM = "account/resetPasswordForm";
    private static final String RESET_PASSWORD_ANSWER_FORM = "account/resetPasswordForm2";
    private static final String RESET_PASSWORD_OK = "account/resetPasswordOk";

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole(USER)")
    public String getAccountManagment(Model model) {
        model.addAttribute("changePasswordForm", new ChangePasswordForm());
        return ACCOUNT_MANAGMENT_PAGE;
    }

    @RequestMapping(value = "/resetuj_haslo", method = RequestMethod.GET)
    @PreAuthorize("isAnonymous()")
    public String getEmailForm(Model model) {
        EmailForm form = new EmailForm();
        model.addAttribute("emailForm", form);
        return RESET_PASSWORD_FORM;
    }

    @RequestMapping(value = "/resetuj_haslo", method = RequestMethod.POST)
    @PreAuthorize("isAnonymous()")
    public String postResetPasswordForm(Model model, @Valid EmailForm emailForm, BindingResult result) {
        try {
            Account account = accountService.findByEmail(emailForm.getEmail());
            if (account == null) {
                result.rejectValue("email", "error.duplicate.email", new String[]{emailForm.getEmail()}, null);
                return RESET_PASSWORD_FORM;
            } else {
                AnswerQuestionForm form = new AnswerQuestionForm();
                form.setId(account.getId());
                form.setQuestion(account.getQuestion());
                model.addAttribute("answerQuestionForm", form);
                return RESET_PASSWORD_ANSWER_FORM;
            }
        } catch (NOMException e) {

        }
        return RESET_PASSWORD_FORM;
    }

    @RequestMapping(value = "/resetuj_haslo2", method = RequestMethod.POST)
    @PreAuthorize("isAnonymous()")
    public String postResetPasswordForm2(Model model, @Valid AnswerQuestionForm answerQuestionForm, BindingResult result) {
        try {
            System.err.println("proba resetu id=" + answerQuestionForm.getId());
            accountService.resetPassword(answerQuestionForm.getId(), answerQuestionForm.getAnswer(), result);
            if (!result.hasErrors())
                return RESET_PASSWORD_OK;
        } catch (NOMException e) {

        }
        return RESET_PASSWORD_ANSWER_FORM;
    }


    @RequestMapping(value = "/zmien_haslo", method = RequestMethod.POST)
    public String changePassword(@Valid ChangePasswordForm changePasswordForm, BindingResult result) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (isUser(authentication)) {
                convertPasswordError(result);
                if (!result.hasErrors()) {
                    Account account = accountService.findByUsername(authentication.getName());
                    boolean isPasswordCorrect = accountService.checkPassword(account, changePasswordForm.getOldPassword());
                    if (isPasswordCorrect) {
                        accountService.changePassword(account, changePasswordForm.getNewPassword());
                    } else {
                        result.rejectValue("oldPassword", "error.wrong.password", new String[]{changePasswordForm.getOldPassword()}, null);
                    }
                }
            }
        } catch (NOMException e) {

        }
        return ACCOUNT_MANAGMENT_PAGE;
    }

    private static void convertPasswordError(BindingResult result) {
        for (ObjectError error : result.getGlobalErrors()) {
            String msg = error.getDefaultMessage();
            if ("account.password.mismatch.message".equals(msg)) {
                if (!result.hasFieldErrors("newPassword")) {
                    result.rejectValue("newPassword", "error.mismatch");
                }
            }
        }
    }
}
