package wieik.nom.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.nom.dao.ArticleDao;
import wieik.nom.dao.SectionDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Article;
import wieik.nom.dto.Section;
import wieik.nom.services.AccountService;
import wieik.nom.services.ArticleService;

import java.util.List;

/**
 * Created by Andrzej on 2015-06-09.
 */

@Controller
@RequestMapping("/panelRedaktoraNaczelnego")
public class accountManagementEditorInChiefController  extends BaseController {
    private static final String CATEGORY_LIST = "/account/categoryList";
    private static final String ACCEPT_CONTENT = "/account/acceptContent";
    private static final String SET_CONTENT_PRIORITY = "/account/setContentPriority";
    private static final String SET_PRIORITY = "/account/setPriority";
    private static final String EDIT_OR_DELETE_CONTENT = "/account/editOrDeleteContent";
    private static final String SET_EDIT_OR_DELETE_CONTENT = "/account/setEditOrDeleteContent";
    private static final String ADD_CATEGORY = "/account/addCategory";
    private static final String ARTICLE_PREVIEW = "/account/articlePreview";

    private ArticleDao articleDao;

    @Autowired(required=true)
    @Qualifier(value="articleDao")
    public void setArticleDao(ArticleDao articleDao){ this.articleDao = articleDao;
    }

    private SectionDao sectionDao;

    @Autowired(required=true)
    @Qualifier(value="sectionDao")
    public void setSectionDao(SectionDao sectionDao){ this.sectionDao = sectionDao;
    }

    @RequestMapping(value = "/akceptujTresc", method = RequestMethod.GET)
    public String getAcceptContent(Model model) {
        model.addAttribute("listSections", sectionDao.getSectionList());
        return CATEGORY_LIST;
    }

    @RequestMapping(value = "/akceptujTresc/", params = {"sectionName"}, method = RequestMethod.GET)
    public String getArticlesInCheckForSection(@RequestParam String sectionName, Model model) {
        Section section = sectionDao.findByName(sectionName);

        model.addAttribute("listArticles", articleDao.getInCheck(section, 1, 10));
        return ACCEPT_CONTENT;
    }

    @RequestMapping(value = "/akceptujTresc/", params = {"articleName", "accept"}, method = RequestMethod.GET)
    public String getAcceptArticleInCheck(@RequestParam String articleName, Model model) {
        articleDao.setAcceptArticleInCheck(articleName);

        return ACCEPT_CONTENT;
    }

    @RequestMapping(value = "/akceptujTresc/", params = {"articleName", "preview"}, method = RequestMethod.GET)
    public String getArticleInCheckPreview(@RequestParam String articleName, Model model) {
        Article article = articleDao.findByName(articleName);

        model.addAttribute("Article", article);

        return ARTICLE_PREVIEW;
    }

    @RequestMapping(value = "/priorytet", method = RequestMethod.GET)
    public String getSetContentPriority(Model model) {
        model.addAttribute("listSections", sectionDao.getSectionList());
        return SET_PRIORITY;
    }

    @RequestMapping(value = "/priorytet", method = RequestMethod.POST)
    public String POSTsetContentPriority(@RequestParam String articleName, @RequestParam String newPriority, Model model) {
        articleDao.setArticlePriority(articleName, newPriority);

        return SET_PRIORITY;
    }

    @RequestMapping(value = "/priorytet/", params = {"sectionName"}, method = RequestMethod.GET)
    public String getSetContentPriorityForSection(@RequestParam String sectionName, Model model) {
        Section section = sectionDao.findByName(sectionName);

        model.addAttribute("listArticles", articleDao.getForSection(section, 1, 10));
        return SET_CONTENT_PRIORITY;
    }

    @RequestMapping(value = "/edytujusuntresc", method = RequestMethod.GET)
    public String getEditOrDeleteContent(Model model) {
        model.addAttribute("listSections", sectionDao.getSectionList());
        return EDIT_OR_DELETE_CONTENT;
    }

    @RequestMapping(value = "/edytujusuntresc/", params = {"sectionName"}, method = RequestMethod.GET)
    public String getSetEditOrDeleteContent(@RequestParam String sectionName, Model model) {
        Section section = sectionDao.findByName(sectionName);

        model.addAttribute("listArticles", articleDao.getForSection(section, 1, 10));

        return SET_EDIT_OR_DELETE_CONTENT;
    }

    @RequestMapping(value = "/edytujusuntresc/", params = {"articleName", "delete"}, method = RequestMethod.GET)
    public String setEditOrDeleteContent(@RequestParam String articleName, Model model) {
        articleDao.deleteArticle(articleName);

        return SET_EDIT_OR_DELETE_CONTENT;
    }

    @RequestMapping(value = "/dodajkategorie", method = RequestMethod.GET)
    public String getAddCategory(Model model) {
        return ADD_CATEGORY;
    }

    @RequestMapping(value = "/dodajkategorie", method = RequestMethod.POST)
    public String postAddCategory(@RequestParam String sectionName, @RequestParam String sectionPriority, @RequestParam String sectionTemplate, Model model) {
        sectionDao.addSection(sectionName, sectionPriority, sectionTemplate);

        return ADD_CATEGORY;
    }
}
