package wieik.nom.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Andrzej on 2015-06-09.
 */

@Controller
@RequestMapping("/panelModeratora")
public class accountManagementModeratorController extends BaseController {
    private static final String MODERATE_COMMENTS = "/account/moderateComments";

    @RequestMapping(value = "/moderowaniekomentarzy", method = RequestMethod.GET)
    public String getModerateComments(Model model) {
        return MODERATE_COMMENTS;
    }
}
