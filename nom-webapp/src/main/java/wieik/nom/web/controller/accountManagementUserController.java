package wieik.nom.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wieik.nom.dao.AccountDao;
import wieik.nom.dto.Account;
import wieik.nom.exceptions.NOMException;
import wieik.nom.web.form.EmailForm;
import wieik.nom.web.form.UserProfileForm;

import javax.validation.Valid;

/**
 * Created by Andrzej on 2015-06-09.
 */

@Controller
@RequestMapping("/panelUzytkownika")
public class accountManagementUserController extends BaseController {
    private static final String EDIT_PROFILE = "/account/editProfile";

    private AccountDao accountDao;

    @Autowired(required=true)
    @Qualifier(value="accountDao")
    public void setAccountDao(AccountDao accountDao){
        this.accountDao = accountDao;
    }

    @RequestMapping(value = "/edycjaprofilu", method = RequestMethod.GET)
    public String getEditProfile(Model model) {
        UserProfileForm userProfileForm = new UserProfileForm();

        model.addAttribute("userProfileForm", userProfileForm);
        return EDIT_PROFILE;
    }

    @RequestMapping(value = "/edycjaprofilu", method = RequestMethod.POST)
    public String postEditProfile(Model model, @Valid UserProfileForm userProfileForm, BindingResult result) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getUsername(); //get logged in username

        Account account = accountDao.getAccountID(name);

        accountDao.UpdateAccountProfile(account.getId(), userProfileForm.getName(), userProfileForm.getSurname(), userProfileForm.getAge(), userProfileForm.getHobby());
        return "redirect:/";
    }
}
