package wieik.nom.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import wieik.nom.dto.Role;
import wieik.nom.exceptions.NOMException;
import wieik.nom.services.*;

/**
 * Created by Sebastian on 2015-03-12.
 * Klasa bazowa dla kontrolerow. Posiada fasade uslug.
 */
public abstract class BaseController {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected SectionService sectionService;

    @Autowired
    protected ArticleService articleService;

    @Autowired
    protected ThemeService themeService;

    @Autowired
    protected EditorService editorService;

    protected boolean isUser(Authentication authentication) throws NOMException {
        boolean authenticated = false;
        for (GrantedAuthority a : authentication.getAuthorities()) {
            if (a.getAuthority().equals(Role.ROLE_USER)) {
                authenticated = true;
            }
        }
        return authenticated;
    }

}
