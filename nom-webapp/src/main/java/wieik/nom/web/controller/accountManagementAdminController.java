package wieik.nom.web.controller;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.nom.dao.AccountDao;
import wieik.nom.dao.RoleDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Role;
import wieik.nom.exceptions.NOMException;
import wieik.nom.services.AccountService;
import wieik.nom.web.form.UserAccountForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Andrzej on 2015-06-09.
 */

@Controller
@RequestMapping("/panelAdmina")
public class accountManagementAdminController extends BaseController {
    private static final String REGISTRATION_OK = "redirect:/konto";
    private static final String MANAGEMENT_PANEL_MAINPAGE = "/account/konto";
    private static final String ADD_ACCOUNT = "/account/addAccount";
    private static final String SET_ROLE = "/account/setRole";
    private static final String BLOCK_OR_DELETE = "/account/blockOrDelete";
    private static final String BLOCK_OR_DELETE_OK = "redirect:/panelAdmina/zablokujusun";
    private static final String EDIT_TEMPLATE = "/account/editTemplate";
    private static final String EDIT_ADVERTISEMENTS = "/account/editAdvertisement";
    private static final String EDIT_ROLE = "/account/editRole";
    private static final String EDIT_OK = "redirect:/panelAdmina/uprawnienia";

    private Logger logger = Logger.getLogger(accountManagementAdminController.class.getName());

    private AccountService accountService;

    @Autowired(required=true)
    @Qualifier(value="accountService")
    public void setAccountService(AccountService accService){
        this.accountService = accService;
    }

    private RoleDao roleDao;

    @Autowired(required=true)
    @Qualifier(value="roleDao")
    public void setRoleDao(RoleDao roleDao){
        this.roleDao = roleDao;
    }

    private AccountDao accountDao;

    @Autowired(required=true)
    @Qualifier(value="accountDao")
    public void setAccountDao(AccountDao accountDao){
        this.accountDao = accountDao;
    }


    @RequestMapping(value = "/dodajkonto", method = RequestMethod.GET)
    public String getAddAccount(Model model) {
        model.addAttribute("userAccountForm", new UserAccountForm());
        return ADD_ACCOUNT;
    }

    @RequestMapping(value = "/dodajkonto", method = RequestMethod.POST)
    public String postAddUserForm(@Valid UserAccountForm userAccountForm, BindingResult result) {
        try {
            convertPasswordError(result);
            String password = userAccountForm.getPassword();
            String answer = userAccountForm.getAnswer();
            accountService.registerUserAccount(toAccount(userAccountForm), password, answer, result);
            return (result.hasErrors() ? ADD_ACCOUNT : REGISTRATION_OK);
        } catch (NOMException e) {

        }
        return "MANAGEMENT_PANEL_MAINPAGE";
    }

    @RequestMapping(value = "/uprawnienia", method = RequestMethod.GET)
    public String getSetRole(Model model) {
        model.addAttribute("account", new Account());
        model.addAttribute("listAccounts", this.accountService.listAccount());
        return SET_ROLE;

      }

    @RequestMapping(value = "/uprawnienia/edytujUprawnienia/", params = "userName", method = RequestMethod.GET)
    public String getEditRole(@RequestParam String userName, Model model) {
        List<Role> roles = new ArrayList<Role>();

        roles = roleDao.getRolesForUser(userName);

        List<Role> allRoles = roleDao.getAllRoles();

        List<Role> diffRoles = ListUtils.subtract(allRoles, roles);

        model.addAttribute("listRoles", roles);
        model.addAttribute("listNewRoles", diffRoles);
        model.addAttribute("user", userName);

        return EDIT_ROLE;
    }

    @RequestMapping(value = "/uprawnienia/edytujUprawnienia/", params = {"userName" ,"removeRoleName"}, method = RequestMethod.GET)
    public String getRemoveRole(@RequestParam String userName, @RequestParam String removeRoleName, Model model) {
        roleDao.removeRoleForUser(userName, removeRoleName);

        return EDIT_OK;
    }

    @RequestMapping(value = "/uprawnienia/edytujUprawnienia/", params = {"userName" ,"addRoleName"}, method = RequestMethod.GET)
    public String getAddRole(@RequestParam String userName, @RequestParam String addRoleName, Model model) {
        roleDao.addRoleForUser(userName, addRoleName);

        return EDIT_OK;
    }

    @RequestMapping(value = "/zablokujusun", method = RequestMethod.GET)
    public String getBlockOrDelete(Model model) {
        model.addAttribute("account", new Account());
        model.addAttribute("listAccounts", this.accountService.listAccount());
        return BLOCK_OR_DELETE;
    }

    @RequestMapping(value = "/zablokujusun/", params = {"userName" ,"block"}, method = RequestMethod.GET)
    public String getBlockUser(@RequestParam String userName, Model model) {
        accountDao.BlockUser(userName);
        return BLOCK_OR_DELETE_OK;
    }

    @RequestMapping(value = "/zablokujusun/", params = {"userName" ,"delete"}, method = RequestMethod.GET)
    public String getDeleteUser(@RequestParam String userName, Model model) {
        accountDao.DeleteUser(userName);
        return BLOCK_OR_DELETE_OK;
    }

    @RequestMapping(value = "/edytujszablon", method = RequestMethod.GET)
    public String getEditTemplate(Model model) {
        return EDIT_TEMPLATE;
    }

    @RequestMapping(value = "/edytujreklamy", method = RequestMethod.GET)
    public String getEditAdvertisements(Model model) {
        return EDIT_ADVERTISEMENTS;
    }

    private static Account toAccount(UserAccountForm form) {
        Account account = new Account();
        account.setUsername(form.getUsername());
        account.setEmail(form.getEmail());
        account.setSex(form.getSex());
        account.setQuestion(form.getQuestion());
        return account;
    }

    private static void convertPasswordError(BindingResult result) {
        for (ObjectError error : result.getGlobalErrors()) {
            String msg = error.getDefaultMessage();
            if ("account.password.mismatch.message".equals(msg)) {
                if (!result.hasFieldErrors("password")) {
                    result.rejectValue("password", "error.mismatch");
                }
            }
        }
    }
}
