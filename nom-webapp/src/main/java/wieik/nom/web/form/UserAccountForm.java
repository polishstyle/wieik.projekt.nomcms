package wieik.nom.web.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;
import wieik.nom.dto.Account;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2014-11-13.
 */

@ScriptAssert(
        lang = "javascript",
        script = "_this.confirmPassword != null && _this.confirmPassword.equals(_this.password)",
        message = "account.password.mismatch.message")
public class UserAccountForm {

    @NotEmpty
    @Size(min = Account.USERNAME_MIN_SIZE, max = Account.USERNAME_MAX_SIZE)
    private String username;

    @NotEmpty
    @Size(min = Account.PASSWORD_MIN_SIZE, max = Account.PASSWORD_MAX_SIZE)
    private String password;

    @NotEmpty
    @Size(min = Account.PASSWORD_MIN_SIZE, max = Account.PASSWORD_MAX_SIZE)
    private String confirmPassword;

    @NotEmpty
    @Size(min = Account.EMAIL_MIN_SIZE, max = Account.EMAIL_MAX_SIZE)
    @Email
    private String email;

    @NotNull
    @Pattern(regexp = Account.SEX_REGEXP, message = "validation.wrong.sex")
    private String sex;

    @NotEmpty
    @Size(max = Account.QUESTION_MAX_SIZE)
    private String question;

    @NotEmpty
    private String answer;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
