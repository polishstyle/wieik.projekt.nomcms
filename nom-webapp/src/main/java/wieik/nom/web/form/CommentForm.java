package wieik.nom.web.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Sebastian on 2015-06-07.
 */
public class CommentForm {

    @NotEmpty
    @Length(min = 6)
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
