package wieik.nom.web.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Sebastian on 2015-03-26.
 */
public class AnswerQuestionForm {

    private long id;

    private String question;

    @NotEmpty
    private String answer;

    public String getQuestion() {
        return question;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
