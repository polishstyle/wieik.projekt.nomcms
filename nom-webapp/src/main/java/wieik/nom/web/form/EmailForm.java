package wieik.nom.web.form;

import org.hibernate.validator.constraints.Email;

/**
 * Created by Sebastian on 2015-03-25.
 */
public class EmailForm {
    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
