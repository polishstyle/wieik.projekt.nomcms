package wieik.nom.web.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;
import wieik.nom.dto.Account;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-06-04.
 */
@ScriptAssert(
        lang = "javascript",
        script = "_this.newPassoword2 != null && _this.newPassoword2.equals(_this.newPassword)",
        message = "account.password.mismatch.message")
public class ChangePasswordForm {
    @NotEmpty
    private String oldPassword;
    @NotEmpty
    @Size(min = Account.PASSWORD_MIN_SIZE, max = Account.PASSWORD_MAX_SIZE)
    private String newPassword;
    @NotEmpty
    @Size(min = Account.PASSWORD_MIN_SIZE, max = Account.PASSWORD_MAX_SIZE)
    private String newPassoword2;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassoword2() {
        return newPassoword2;
    }

    public void setNewPassoword2(String newPassowrd2) {
        this.newPassoword2 = newPassowrd2;
    }
}
