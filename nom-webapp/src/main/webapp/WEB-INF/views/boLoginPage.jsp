<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<c:url var="postLoginUrl" value="/j_spring_security_check"/>

<html>
<head>
    <title><spring:message code="loginPage.title"/></title>
</head>
<body>

<h1><spring:message code="loginPage.title"/></h1>

<form action="${postLoginUrl}" method="post">
    <c:if test="${param.failed == true}">
        <spring:message code="loginPage.failed"/>
    </c:if>
    <spring:message code="loginPage.username"/>
    <input type="text" name="j_username"/>
    <br/>
    <spring:message code="loginPage.password"/>
    <input type="password" name="j_password"/>
    <br/>

    <input type="checkbox" name="_spring_security_remember_me"/> <spring:message code="loginPage.rememberMe"/>
    <br/>
    <input type="submit" value="<spring:message code="loginPage.login" />"/>
</form>
</body>
</html>