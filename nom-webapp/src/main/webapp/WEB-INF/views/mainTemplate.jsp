<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%-- szablon strony, w odpowiednie miejsce wstawiany jest fragment odpowiadajacy za wlasciwa tresc strony --%>

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<c:set var="pageTitle" value="${pageTitle}"/>
<c:url var="themeCss" value="${themeCssUrl}"/>


<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <c:out value="${pageTitle}"/>
    </title>
    <%-- nie chce pojsc przez jsp z kodem css i trzeba tutaj wstawic :( --%>
    <c:if test="${editorMode}">
        <script src="/NOM/resources/ckeditor/ckeditor.js"></script>
    </c:if>

    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>

<%-- GORNY PASEK --%>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"/>NOM-Info</a>

        </div>

        <%@ include file="subhead.jspf" %>

    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <%-- MENU SEKCJI --%>
            <ol class="breadcrumb">
                <c:forEach var="sectionName" items="${sectionNames}" varStatus="loop">
                    <c:if test="${loop.index>0}">&nbsp;|&nbsp;</c:if>
                    <li></li>
                    <a href="<c:out value="${sectionsLink}"/><c:out value="${sectionName}"/>"><c:out
                            value="${sectionName}"/></a></li>
                </c:forEach>
            </ol>

            <%-- TRESC - plik dolaczany dynamicznie --%>
            <div class="content">
                <jsp:include page="${contentViewName}"/>
            </div>


            <%-- POGODA --%>
            <div class="row">
                <div class="col-md-6">
                    <div align="center">
                        <div id="PogodaNetWidget" style="width:600px">
                            <a href="http://pogoda.net" rel="nofollow">pogoda.net</a>
                        </div>
                        <script type="text/javascript" charset="utf-8"
                                src="http://pogoda.net/widgets/js_v2?format=horizontal&limit=3&pid=379"
                                async="async"></script>
                    </div>
                </div>

                <%-- REKLAMA nr 1 --%>
                <div class="col-md-6">
                    <div align="center">
                        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                                codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0"
                                width="468" height="60" id="polskieserce468_01" align="">
                            <param name="movie"
                                   value="http://polskieserce.pl/bannery/polskieserce468_01.swf">
                            <param name="quality" value="high">
                            <param name="bgcolor" value="#ffffff">
                            <embed src="http://polskieserce.pl/bannery/polskieserce468_01.swf"
                                   quality="high"
                                   bgcolor="#ffffff" width="468" height="60" name="polskieserce120x50"
                                   align=""
                                   type="application/x-shockwave-flash"
                                   pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                        </object>
                    </div>
                </div>
            </div>
            <br/>

            <%-- NAJNOWSZE Z SEKCJI --%>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">NAJNOWSZE</h3>
                </div>
                <div class="panel-body">
                    <c:forEach var="link" items="${newestLinks}" varStatus="loop">
                        <a href="<c:out value=" ${link.link}"/>">
                            <c:out value="${link.title}"/>
                        </a><br/>
                        <img height="24" width="24" src="/NOM/img/png/<c:out value=" ${link.imageName}"/>"><br/>
                        <c:out value="${link.description}"/>
                        <br/><br/>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="footer" class="container">
                    <nav class="navbar navbar-default">
                        <div class="navbar-inner navbar-content-center">
                            <%@ include file="footer.jspf" %>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>