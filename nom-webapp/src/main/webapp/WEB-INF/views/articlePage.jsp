<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<%-- ARTYKUL --%>
<div class="panel panel-default">
    <div class="panel-body">

        <h2><c:out value="${article.title}"/></h2>

        <p>
            <small>DATA PUBLIKACJI: <c:out value="${article.publicationDate}"/></small>
        <hr/>
        </p>
        <p style="font-size: 16px"><c:out value="${article.content}"/>
        <hr/>
        </p>


        <%-- Jesli jestesmy w trybie edycjo mozemy edytowac, przydalby sie jakis guzik pokazujacy/chowajacy edytor i tekst --%>
        <%-- TODO; walidacja pol --%>
        <c:if test="${editorMode}">
            <form method="POST" action="/NOM/art" enctype="multipart/form-data">
                <div class="form-group">
                    <input class="form-control" name="sectionName" type="hidden"
                           value="<c:out value="${sectionName}"/>">
                    <input name="oldName" type="hidden" value="<c:out value="${article.name}"/>">
                </div>
                <div class="form-group">
                    Nazwa (tylko litery, uzywana w linku): <input class="form-control" name="name" type="text"
                                                                  value="<c:out value="${article.name}"/>"></div>
                <div class="form-group">
                    Tytul: <input class="form-control" name="title" type="text"
                                  value="<c:out value="${article.title}"/>"></div>
                <div class="form-group">
                    Skrot (widoczne w menu krotkie streszczenie): <input class="form-control" name="preview" type="text"
                                                                         value="<c:out value="${article.preview}"/>">
                </div>
                <div class="form-group">
                    Nazwa motywu: <input class="form-control" name="themeName" type="text"
                                         value="<c:out value="${themeName}"/>"></div>
                <div class="form-group">
                    Obrazek do artykulu: <input type="file" name="image"></div>

        <textarea name="content" id="editor1" rows="10" cols="80">
            <c:out value="${article.content}"/>
        </textarea><br/>
                <script>
                    CKEDITOR.replace('editor1');
                </script>
                <input class="btn btn-lg btn-success btn-sm" type="submit" value="OK">
            </form>
        </c:if>
        <br/>

        <p>AUTOR: <c:out value="${article.author.firstName}"/> <c:out value="${article.author.lastName}"/></p>

        <p>OCENY:</p>

        <p>TAK: <c:out value="${article.votesYes}"/></p>

        <p>NIE: <c:out value="${article.votesNo}"/></p>


        <%-- OCENIANIE --%>
        <security:authorize access="hasRole('USER')">
            <a class="label label-primary" href="<c:out value="${voteUrl}" />/1">TAK</a> | <a class="label label-danger"
                                                                                              href="<c:out value="${voteUrl}"/>/0">NIE</a>
        </security:authorize>
        <security:authorize access="isAnonymous()">
        <p><a href="/NOM/zaloguj" class="label label-success">ZALOGUJ SIE ABY OCENIC!</a>
            </security:authorize>
    </div>
</div>

<%-- PORTALE SPOLECZNOSCIOWE --%>
<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>

<%-- REKLAMA nr 2 --%>
<div align="center">
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
            codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="468"
            height="60" id="polskieserce468_02_grosz" align="">
        <param name="movie" value="http://polskieserce.pl/bannery/polskieserce468_02_grosz.swf">
        <param name="quality" value="high">
        <param name="bgcolor" value="#ffffff">
        <embed src="http://polskieserce.pl/bannery/polskieserce468_02_grosz.swf" quality="high" bgcolor="#ffffff"
               width="468" height="60" name="polskieserce120x50" align="" type="application/x-shockwave-flash"
               pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
    </object>
</div>

<%-- KOMENTARZE --%>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Komentarze</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped">
            <c:forEach var="comment" items="${comments}" varStatus="loop">
                <tr>
                    <td><a href="/NOM/userProfile?userName=${comment.user.username}"><c:out
                            value="${comment.user.username}"/></a></td>
                    <td><c:out value="${comment.createDate}"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <c:out value="${comment.content}"/>
                        <security:authorize access="hasRole('MODERATOR')">
                            <br/>
                            <a href="/NOM/p/${article.getSection().getName()}/${article.getName()}?deleteComment=true&commentContent=${comment.getContent()}"
                               class="alert-link">Usun komentarz</a>
                        </security:authorize>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
<c:if test="${pages>1}">
    <c:forEach begin="${pages-(pages-1)}" end="${pages}" var="i">
        <c:if test="${currentPage==i}">
            <a style="color: red;" href="<c:out value="${commentPageUrl}"/>${i}"> ${i} </a>
        </c:if>
        <c:if test="${currentPage!=i}">
            <a href="<c:out value="${commentPageUrl}"/>${i}"> ${i} </a>
        </c:if>
    </c:forEach>
</c:if>

<%-- DODAWANIE KOMENTARZA --%>
<security:authorize access="hasRole('USER')">
    <div class="form-inline">
        <form action="${addCommentUrl}" method="POST">
            <input type="text" name="comment" class="form-control">
            <input type="submit" value="DODAJ KOMENTARZ" class="btn btn-success">
        </form>
    </div>
    <br/>
</security:authorize>
<security:authorize access="isAnonymous()">
    <div class="alert alert-success" role="alert"><a href="/NOM/zaloguj" class="alert-link">ZALOGUJ SIE</a> ABY DODAC
        KOMENTARZ!
    </div>
</security:authorize>
