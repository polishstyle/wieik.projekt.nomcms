<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<div class="panel panel-default">
    <div class="panel-body">
        <h2><c:out value="${sectionName}"/></h2>
        <%-- TODO: opakowac linki w css --%>

        <%-- LINKI DO NAJWAZNIEJSZYCH NAJNOWSZYCH ARTYKULOW - priorytet 4, bez stronicowania --%>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">NAJWAZNIEJSZE ARTYKULY</h3>
            </div>
            <div class="panel-body">
                <c:forEach var="link" items="${topLinks}" varStatus="loop">
                    <h5><a href="<c:out value="${link.link}"/>"><c:out value="${link.title}"/></a></h5>

                    <p><c:out value="${link.description}"/></p>
                    <hr/>
                </c:forEach>
            </div>
        </div>

        <%-- LINKI DO RESZTY ARTYKULOW - rozmiar zalezny od priorytetu artykulu (od 1 do 4) --%>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">POZOSTALE ARTYKULY</h3>
            </div>
            <div class="panel-body">

                <c:forEach var="link" items="${sectionLinks}" varStatus="loop">
                    <h5><a href="<c:out value="${link.link}"/>"><c:out value="${link.title}"/></a></h5>

                    <p><c:out value="${link.description}"/></p>
                    <hr/>
                </c:forEach>
            </div>
        </div>
        <%-- Link do wczesniejszej i/lub kolejnej strony --%>

        <c:if test="${prevPage}">
            <a href="<c:out value="${sectionPageUrl}"/>${currentPage-1}"> NOWSZE </a>
        </c:if>
        <c:if test="${nextPage}">
            <a href="<c:out value="${sectionPageUrl}"/>${currentPage+1}"> STARSZE </a>
        </c:if>
    </div>
</div>