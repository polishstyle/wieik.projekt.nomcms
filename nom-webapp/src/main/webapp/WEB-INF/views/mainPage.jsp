<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%-- szablon strony, w odpowiednie miejsce wstawiany jest fragment odpowiadajacy za wlasciwa tresc strony --%>

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<c:set var="pageTitle" value="${pageTitle}"/>
<%--<c:url var="themeCss" value="${themeCssUrl}" />--%>


<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%-- <link href="<c:out value="${themeCss}" />" type="text/css" rel="stylesheet"> --%>
    <%-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> --%>

    <%-- nie chce pojsc przez jsp z kodem css i trzeba tutaj wstawic :( --%>

    <%--STARY CSS
    <style type="text/css">
        <c:out value="${style}" />
    </style>--%>

    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>
<%-- GORNY PASEK --%>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"/>NOM-Info</a>
        </div>
        <%@ include file="subhead.jspf" %>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <%-- TOP ARTYKULOW Z NAJWAZNIEJSZYCH SEKCJI --%>
                            <h3 class="panel-title">NAJWAZNIEJSZE WYDARZENIA</h3
                        </div>
                        <c:forEach var="link" items="${topLinks}" varStatus="loop">
                            <div class="panel-body">
                                <a href="<c:out value=" ${link.link}"/>">
                                    <c:out value="${link.title}"/>
                                </a><br/>
                                <img height="42" width="42" src="/NOM/img/png/<c:out value=" ${link.imageName}"/>"><br/>
                                <c:out value="${link.description}"/>
                                <br/>-----------------<br/>
                            </div>
                        </c:forEach>
                    </div>
                </div>

                <%-- REKLAMA 2 --%>
                <div align="center">
                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                            codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0"
                            width="468" height="60" id="polskieserce468_01" align="">
                        <param name="movie" value="http://polskieserce.pl/bannery/polskieserce468_01.swf">
                        <param name="quality" value="high">
                        <param name="bgcolor" value="#ffffff">
                        <embed src="http://polskieserce.pl/bannery/polskieserce468_01.swf" quality="high"
                               bgcolor="#ffffff" width="468" height="60" name="polskieserce120x50" align=""
                               type="application/x-shockwave-flash"
                               pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                    </object>
                </div>

                <%-- SEKCJE--%>
                <c:forEach var="section" items="${topLinksForSections}" varStatus="loop">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><a href="<c:out value=" ${sectionUrl}"/>
                                    <c:out value="${section.name}"/>
                                    ">
                                <c:out value="${section.name}"/>
                            </a></h3>
                        </div>
                            <%-- NAJWAZNIEJSZE ARTYKULY DLA SEKCJI--%>
                        <c:forEach var="link" items="${section.links}" varStatus="loop">
                            <div class="panel-body">
                                <h5><a href="<c:out value=" ${link.link}"/>">
                                    <c:out value="${link.title}"/>
                                </a></h5>
                                <img height="42" width="42" src="/NOM/img/png/<c:out value=" ${link.imageName}"/>">

                                <p></p>

                                <p><c:out value="${link.description}"/></p>
                            </div>
                            <hr/>
                        </c:forEach>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">

                <%-- POGODA --%>
                <%--<div class="alert alert-info" role="alert">--%>
                <div align="center">
                    <div id="PogodaNetWidget" style="width:300px">
                        <a href="http://pogoda.net" rel="nofollow">pogoda.net</a>
                    </div>
                    <script type="text/javascript" charset="utf-8"
                            src="http://pogoda.net/widgets/js_v2?format=vertical&width=250&limit=1&pid=379"
                            async="async"></script>
                </div>
                <%--</div>--%>
                <br/>

                <div align="center">
                    <%-- REKLAMA nr 1 --%>
                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                            codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0"
                            width="120" height="120" id="polskieserce120x120_02_grosz" align="">
                        <param name="movie"
                               value="http://polskieserce.pl/bannery/polskieserce120x120_02_grosz.swf">
                        <param name="quality" value="high">
                        <param name="bgcolor" value="#ffffff">
                        <embed src="http://polskieserce.pl/bannery/polskieserce120x120_02_grosz.swf"
                               quality="high" bgcolor="#ffffff" width="120" height="120"
                               name="polskieserce120x50" align="" type="application/x-shockwave-flash"
                               pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                    </object>
                </div>
            </div>

            <%-- SEKCJE I LINKI DO ARTYKULOW (male) --%>

            <c:forEach var="section" items="${sections}" varStatus="loop">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="<c:out value=" ${sectionUrl}"/>
                                    <c:out value="${section.name}"/>">
                            <c:out value="${section.name}"/>
                        </a></h3>
                    </div>
                        <%-- NAJWAZNIEJSZE ARTYKULY DLA SEKCJI--%>
                    <c:forEach var="link" items="${section.links}" varStatus="loop">
                        <div class="panel-body">
                            <h5><a href="<c:out value=" ${link.link}"/>">
                                <c:out value="${link.title}"/>
                            </a></h5>
                            <img height="24" width="24" src="/NOM/img/png/<c:out value=" ${link.imageName}"/>">

                            <p></p>

                            <p><c:out value="${link.description}"/></p>
                        </div>
                        <hr/>
                    </c:forEach>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<%-- STOPKA --%>
<div class="row">
    <div class="col-md-12">
        <div id="footer" class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-inner navbar-content-center">
                    <%@ include file="footer.jspf" %>
                </div>
            </nav>
        </div>
    </div>
</div>
</div>
</div>
</body>
</html>