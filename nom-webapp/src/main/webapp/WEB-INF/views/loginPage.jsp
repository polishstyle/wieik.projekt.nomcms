<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<c:url var="postLoginUrl" value="/j_spring_security_check"/>
<c:url var="resetPassword" value="/konto/resetuj_haslo"/>
<c:url var="reactivateAccount" value="/rejestracja/ponow_aktywacje"/>

<html>
<head>
    <title><spring:message code="loginPage.title"/></title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<%-- GORNY PASEK --%>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"/>NOM-Info</a>
        </div>

        <%@ include file="subhead.jspf" %>

    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><spring:message code="loginPage.title"/></h3>
                </div>
                <div class="panel-body">
                    <form action="${postLoginUrl}" method="post">
                        <c:if test="${param.failed == true}">
                            <spring:message code="loginPage.failed"/>
                        </c:if>
                        <div class="form-group">
                            <spring:message code="loginPage.username"/>
                            <input type="text" name="j_username" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="loginPage.password"/>
                            <input type="password" name="j_password" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <input type="checkbox" name="_spring_security_remember_me" class="checkbox-inline"/>
                            <spring:message
                                    code="loginPage.rememberMe"/>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="<spring:message code="loginPage.login" />"
                                   class="btn btn-lg btn-success"/>
                        </div>

                    </form>

                    <a href="${resetPassword}">Nie pamietam hasla</a><br>
                    <a href="${reactivateAccount}">Ponowna aktywacja konta</a><br>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>