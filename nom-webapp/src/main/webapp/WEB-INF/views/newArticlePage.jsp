<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%-- szablon strony, w odpowiednie miejsce wstawiany jest fragment odpowiadajacy za wlasciwa tresc strony --%>

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<c:set var="pageTitle" value="${pageTitle}"/>
<c:url var="themeCss" value="${themeCssUrl}"/>


<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%-- <link href="<c:out value="${themeCss}" />" type="text/css" rel="stylesheet"> --%>
    <%-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> --%>
    <%-- nie chce pojsc przez jsp z kodem css i trzeba tutaj wstawic :( --%>
    <script src="/NOM/resources/ckeditor/ckeditor.js"></script>

    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<%-- GORNY PASEK --%>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"/>NOM-Info</a>
        </div>
        <%@ include file="subhead.jspf" %>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <%-- TRESC - plik dolaczany dynamicznie --%>

                    <form method="POST" action="/NOM/art" enctype="multipart/form-data">
                        <%-- TODO: sekcje z selecta, tylko te, w ktorych jestesmy redaktorami --%>
                        <div class="form-group">
                            Nazwa sekcji: <input class="form-control" name="sectionName" type="text" value="<c:out value="${sectionName}"/>">
                            <input name="oldName" type="hidden" value="<c:out value="${name}"/>">
                        </div>
                        <div class="form-group">
                            Nazwa (tylko litery, uzywana w linku): <input class="form-control" name="name" type="text"
                                                                          value="<c:out value="${name}"/>"></div>
                        <div class="form-group">
                            Tytul: <input class="form-control" name="title" type="text" value="<c:out value="${title}"/>"></div>
                        <div class="form-group">
                            Skrot (widoczne w menu krotkie streszczenie): <input class="form-control" name="preview" type="text"
                                                                                 value="<c:out value="${preview}"/>">
                        </div>
                        <%-- TODO: motywy z selecta --%>
                        <div class="form-group">
                            Nazwa motywu: <input class="form-control" name="themeName" type="text" value="<c:out value="${themeName}"/>">
                        </div>
                        <div class="form-group">
                            Obrazek do artykulu: <input type="file" name="image"></div>
                <textarea name="content" id="editor1" rows="10" cols="80">
                    <c:out value="${article.content}"/>
                </textarea><br/>
                        <script>
                            CKEDITOR.replace('editor1');
                        </script>
                        <input type="submit" class="btn btn-lg btn-success btn-sm" value="OK">
                        <br/>
                    </form>
                </div>
            </div>
        </div>

        <%-- STOPKA --%>
        <div class="row">
            <div class="col-md-12">
                <div id="footer" class="container">
                    <nav class="navbar navbar-default">
                        <div class="navbar-inner navbar-content-center">
                            <%@ include file="footer.jspf" %>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
