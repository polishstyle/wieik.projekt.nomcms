<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<spring:message var="pageTitle" code="addCategory.pageTitle"/>


<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><c:out value="${pageTitle}"/></h3>
                </div>
                <div class="panel-body">
                    <form action="/NOM/panelRedaktoraNaczelnego/dodajkategorie" method="POST">

                        Nazwa sekcji: <input type="text" name="sectionName" class="form-control"><br/>
                        Priorytet sekcji: <input type="text" name="sectionPriority" class="form-control"><br/>
                        Szablon sekcji: <input type="text" name="sectionTemplate" class="form-control"><br/>

                        <input type="submit" value="Dodaj sekcje" class="btn btn-lg btn-primary"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>