<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- zmienne --%>
<c:url var="submitRegistrationUrl" value="/panelRedaktoraNaczelnego/akceptujTresc"/>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="acceptContent.pageTitle"/>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>
    <div class="row">
        <div class="col-md-8">
            <h1><c:out value="${pageTitle}"/></h1>

            <div class="panel panel-primary">
                <div class="panel-heading">Lista Kategorii</div>
                <c:if test="${empty listSections}">
                    <h1>
                        Brak Sekcji w systemie.
                    </h1>
                </c:if>

                <c:if test="${!empty listSections}">
                    <table class="table">
                        <tr>
                            <th>Nazwa sekcji</th>
                            <th>Priorytet</th>
                            <th>Opcje</th>
                        </tr>
                        <c:forEach items="${listSections}" var="section">
                            <tr>
                                <td>${section.getName()}</td>
                                <td>${section.getPriority()}</td>
                                <td>
                                    <a href="<c:url value='/panelRedaktoraNaczelnego/akceptujTresc/?sectionName=${section.getName()}' />">Zobacz
                                        oczekujące artykuły</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
        </div>
    </div>
</div>
</body>
</html>