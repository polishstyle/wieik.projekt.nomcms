<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<spring:message var="pageTitle" code="subhead.accountManagment"/>
<c:url var="submitChangePasswordUrl" value="/konto/zmien_haslo"/>
<spring:message var="submitChangePassword" code="accountManagment.label.submit"/>
<spring:message var="changePassword" code="AccountManagementHead.changePassword"/>

<%-- CSS --%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><c:out value="${pageTitle}"/></title>
</head>
<body>

<div class="container">

  <h1> Profil uzytkownika ${userName}</h1>
  <br/>
  <br/>
  <h1> Imię: ${Name}</h1>
  <h1> Nazwisko: ${Surname}</h1>
  <h1> Wiek: ${Age}</h1>
  <h1> Hobby: ${Hobby}</h1>

</div>

</body>
</html>