<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- zmienne --%>
<c:url var="submitRegistrationUrl" value="/panelRedaktoraNaczelnego/akceptujTresc"/>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="acceptContent.pageTitle"/>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9
        }
    </style>
    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>

    <h1><c:out value="${pageTitle}"/></h1>


    <c:if test="${empty Article}">
        <div class="panel panel-primary">
            <h1>
                Błąd, podgląd niemożliwy.
            </h1>
        </div>
    </c:if>

    <c:if test="${!empty Article}">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Autor artykułu: ${Article.getAuthor()}
            </div>
            <div class="panel-body">
                <h1>
                    Nazwa artykułu: ${Article.getName()}
                </h1>
                <hr/>
                <h1>
                    Tytuł artykułu: ${Article.getTitle()}
                </h1>
                <hr/>
                <h1>
                    Treść artykułu:
                </h1>

                <p> ${Article.getContent()}</p>

                    <%--
                    <table class="tg">
                      <tr>
                        <th width="120">Autor</th>
                        <th width="120">Nazwa</th>
                        <th width="60">Tytuł</th>
                        <th width="60"></th>
                        <th width="120"></th>
                      </tr>
                      <c:forEach items="${listArticles}" var="article">
                        <tr>
                          <td>${article.getAuthor()}</td>
                          <td>${article.getName()}</td>
                          <td>${article.getTitle()}</td>
                          <td><a href="<c:url value='/panelRedaktoraNaczelnego/akceptujTresc/?articleName=${article.getName()}&preview=true' />" >Podgląd</a></td>
                          <td><a href="<c:url value='/panelRedaktoraNaczelnego/akceptujTresc/?articleName=${article.getName()}&accept=true' />" >Akceptuj</a></td>
                        </tr>
                      </c:forEach>
                    </table>
                    --%>
            </div>
        </div>
    </c:if>

</div>
</body>
</html>