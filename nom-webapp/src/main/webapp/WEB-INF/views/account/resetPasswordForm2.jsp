<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<c:url var="postResetPasswordUrl" value="/konto/resetuj_haslo2" />
<spring:message var="submit" code="resetPasswordForm.label.submit" />

<html>
	<head>
		<title><spring:message code="resetPasswordForm.title" /></title>
	</head>
	<body>
		<h1><spring:message code="resetPasswordForm.title" /></h1>
		<c:if test="${not empty errorMsg}">
                <spring:message code="errorInfo.error" /><c:out value="${errorMsg}" /><br/>
            </c:if>

        <form:form action="${postResetPasswordUrl}" modelAttribute="answerQuestionForm">
   	    <form:errors path="*">
   			<spring:message code="error.global" />
   		</form:errors>

   		<form:hidden path="id"/>
    <spring:message code="resetPasswordForm.label.question" />
        <c:out value="${answerQuestionForm.question}" /><br/>
   		<br/>
   		<spring:message code="resetPasswordForm.label.answer" />
        <form:input path="answer" />
        <form:errors path="answer" htmlEscape="false" />

        <br/>
        <input type="submit" value="${submit}"></input>
	</form:form>

	</body>
</html>