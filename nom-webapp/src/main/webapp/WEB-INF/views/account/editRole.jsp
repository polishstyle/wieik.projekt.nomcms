<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- zmienne --%>
<c:url var="submitRegistrationUrl" value="/panelAdmina/uprawnienia"/>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="setRole.pageTitle"/>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>

    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>
    <div class="row">
        <div class="col-md-6">
            <h1><c:out value="${pageTitle}"/></h1>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    Uprawnienia użytkownika ${user}</div>
                <c:if test="${empty listRoles}">
                    <div class="panel-body">
                        Brak uprawnień dla użytkownika.
                    </div>
                </c:if>

                <c:if test="${!empty listRoles}">

                    <table class="table">
                        <tr>
                            <th>Nazwa uprawnienia</th>
                            <th>Opcje</th>
                        </tr>
                        <c:forEach items="${listRoles}" var="role">
                            <tr>
                                <td>${role.getName()}</td>
                                <td>
                                    <a href="<c:url value='/panelAdmina/uprawnienia/edytujUprawnienia/?userName=${user}&removeRoleName=${role.getName()}' />">Usun
                                        uprawnienie</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Dodaj nowe uprawnienia dla użytkownika ${user}</div>
                <c:if test="${empty listNewRoles}">
                    <div class="panel-body">
                        Użytkownik posiada wszystkie możliwe uprawnienia.
                    </div>
                </c:if>


                <c:if test="${!empty listNewRoles}">
                    <table class="table">
                        <tr>
                            <th>Nazwa uprawnienia</th>
                            <th>Opcje</th>
                        </tr>
                        <c:forEach items="${listNewRoles}" var="role2">
                            <tr>
                                <td>${role2.name}</td>
                                <td>
                                    <a href="<c:url value='/panelAdmina/uprawnienia/edytujUprawnienia/?userName=${user}&addRoleName=${role2.getName()}' />">Dodaj
                                        uprawnienie</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
        </div>
    </div>
</div>
</body>
</html>