<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- zmienne --%>
<c:url var="submitRegistrationUrl" value="/rejestracja"/>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="registrationForm.pageTitle"/>
<spring:message var="msgAllFieldsRequired" code="registrationForm.message.allFieldsRequired"/>
<spring:message var="register" code="registrationForm.label.register"/>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><c:out value="${pageTitle}"/></h3>
                </div>
                <div class="panel-body">
                    <form:form action="${submitRegistrationUrl}" modelAttribute="userAccountForm">
                        <form:errors path="*">
                            <spring:message code="error.global"/>
                        </form:errors>
                        <br/>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.username"/>
                            <form:input path="username" class="form-control"/>
                            <form:errors path="username" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.password"/>
                            <form:password path="password" showPassword="false" class="form-control"/>
                            <form:errors path="password" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.confirmPassword"/>
                            <form:password path="confirmPassword" showPassword="false" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.email"/>
                            <form:input path="email" class="form-control"/>
                            <form:errors path="email" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.sex"/>
                            <form:radiobutton path="sex" value="M" class="checkbox-inline"/><spring:message
                                code="registrationForm.label.male"/>
                            <form:radiobutton path="sex" value="F" class="checkbox-inline"/><spring:message
                                code="registrationForm.label.female"/>
                            <form:errors path="sex" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.question"/>
                            <form:input path="question" class="form-control"/>
                            <form:errors path="question" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="registrationForm.label.answer"/>
                            <form:input path="answer" class="form-control"/>
                            <form:errors path="answer" htmlEscape="false"/>
                        </div>

                        <input type="submit" value="${register}" class="btn btn-lg btn-primary"></input>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>