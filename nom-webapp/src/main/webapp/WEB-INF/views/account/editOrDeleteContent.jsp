<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<spring:message var="pageTitle" code="editOrDeleteContent.pageTitle"/>


<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>
    <div class="row">
        <div class="col-md-10">
            <h1><c:out value="${pageTitle}"/></h1>

            <div class="panel panel-primary">
                <div class="panel-heading">Lista Kategorii</div>
                <c:if test="${empty listSections}">
                    <div class="panel-body">
                        Brak Sekcji w systemie.
                    </div>
                </c:if>

                <c:if test="${!empty listSections}">
                    <table class="table">
                        <tr>
                            <th>Nazwa sekcji</th>
                            <th>Priorytet</th>
                            <th>Opcje</th>
                        </tr>
                        <c:forEach items="${listSections}" var="section">
                            <tr>
                                <td>${section.getName()}</td>
                                <td>${section.getPriority()}</td>
                                <td>
                                    <a href="<c:url value='/panelRedaktoraNaczelnego/edytujusuntresc/?sectionName=${section.getName()}' />">Zobacz
                                        wszystkie artykuły</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
        </div>
    </div>
</div>
</body>
</html>