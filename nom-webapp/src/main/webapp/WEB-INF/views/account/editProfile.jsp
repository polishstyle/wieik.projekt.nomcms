<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<spring:message var="pageTitile" code="editProfile.pageTitle"/>
<c:url var="submitRegistrationUrl" value="edycjaprofilu"/>
<spring:message var="editProfile" code="editProfile.label.edit"/>


<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <c:out value="${pageTitle}"/></div>

                <div class="panel-body">
                    <form:form action="${submitRegistrationUrl}" modelAttribute="userProfileForm">
                        <form:errors path="*">
                            <spring:message code="error.global"/>
                        </form:errors>

                        <div class="form-group">
                            <spring:message code="editProfile.label.name"/>
                            <form:input path="Name" class="form-control"/>
                            <form:errors path="Name" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="editProfile.label.surname"/>
                            <form:input path="Surname" class="form-control"/>
                            <form:errors path="Surname" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="editProfile.label.age"/>
                            <form:input path="Age" class="form-control"/>
                            <form:errors path="Age" htmlEscape="false"/>
                        </div>

                        <div class="form-group">
                            <spring:message code="editProfile.label.hobby"/>
                            <form:input path="Hobby" class="form-control"/>
                            <form:errors path="Hobby" htmlEscape="false"/>
                        </div>

                        <input type="submit" value="${editProfile}" class="btn btn-lg btn-primary">
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>