<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<c:url var="postResetPasswordUrl" value="/konto/resetuj_haslo"/>
<spring:message var="submit" code="resetPasswordForm.label.submit"/>

<html>
<head>
    <title><spring:message code="resetPasswordForm.title"/></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><spring:message code="resetPasswordForm.title"/></h3>
                </div>
                <div class="panel-body">
                    <form:form action="${postResetPasswordUrl}" modelAttribute="emailForm">
                        <form:errors path="*">
                            <spring:message code="error.global"/>
                        </form:errors>

                        <div class="form-group">
                            <spring:message code="resetPasswordForm.label.email"/>
                            <form:input path="email" class="form-control"/>
                            <form:errors path="email" htmlEscape="false"/>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="${submit}" class="btn btn-lg btn-danger"></input>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>