<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- zmienne --%>
<c:url var="submitRegistrationUrl" value="/panelAdmina/uprawnienia"/>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="setRole.pageTitle"/>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
    <%--  <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-4eph{background-color:#f9f9f9}
      </style>--%>
    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <%@ include file="AccountManagementHead.jspf" %>
    <div class="row">
        <div class="col-md-10">
            <h2><c:out value="${pageTitle}"/></h2>

            <div class="panel panel-primary">
                <div class="panel-heading">Lista użytkowników</div>
                <c:if test="${empty listAccounts}">
                    <div class="panel-heading">
                        Brak Użytkowników w systemie.
                    </div>
                </c:if>

                <c:if test="${!empty listAccounts}">
                    <table class="table">
                        <tr>
                            <th>Nazwa użytkownika</th>
                            <th>Email</th>
                            <th>Płeć</th>
                            <th>Edycja uprawnień</th>
                        </tr>
                        <c:forEach items="${listAccounts}" var="account">
                            <tr>
                                <td>${account.username}</td>
                                <td>${account.email}</td>
                                <td>${account.sex}</td>
                                <td>
                                    <a href="<c:url value='/panelAdmina/uprawnienia/edytujUprawnienia/?userName=${account.username}' />">Edytuj uprawnienia</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
        </div>
    </div>
</body>
</html>