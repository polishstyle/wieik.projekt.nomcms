<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- ZMIENNE --%>
<spring:message var="pageTitle" code="subhead.accountManagment"/>
<c:url var="submitChangePasswordUrl" value="/konto/zmien_haslo"/>
<spring:message var="submitChangePassword" code="accountManagment.label.submit"/>
<spring:message var="changePassword" code="AccountManagementHead.changePassword"/>

<%-- CSS --%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<%-- STRONA --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><c:out value="${pageTitle}"/></title>
</head>
<body>

<div class="container">

    <%@ include file="AccountManagementHead.jspf" %>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <c:out value="${changePassword}"/></h3>
                </div>
                <div class="panel-body">
                    <form:form action="${submitChangePasswordUrl}" modelAttribute="changePasswordForm">
                        <form:errors path="*">
                            <spring:message code="error.global"/>
                        </form:errors>
                        <div class="form-group">
                            <spring:message code="accountManagment.label.oldPassword"/>
                            <form:input path="oldPassword" class="form-control"/>
                            <form:errors path="oldPassword" htmlEscape="false"/>
                        </div>
                        <div class="form-group">
                            <spring:message code="accountManagment.label.newPassword"/>
                            <form:input path="newPassword" class="form-control"/>
                            <form:errors path="newPassword" htmlEscape="false"/>
                        </div>
                        <div class="form-group">
                            <spring:message code="accountManagment.label.newPassoword2"/>
                            <form:input path="newPassoword2" class="form-control"/>
                            <form:errors path="newPassoword2" htmlEscape="false"/>
                        </div>
                        <input type="submit" value="${submitChangePassword}" class="btn btn-lg btn-primary"></input>
                    </form:form>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</body>
</html>