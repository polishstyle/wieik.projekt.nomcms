<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<%-- zmienne --%>
<c:url var="submitActivationUrl" value="/rejestracja/aktywacja" />

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="activationForm.pageTitle" />
<spring:message var="msgAllFieldsRequired" code="activationForm.message.allFieldsRequired" />
<spring:message var="register" code="activationForm.label.activation" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>

    <%-- CSS --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <h1><c:out value="${pageTitle}" /></h1>
    <c:if test="${not empty errorMsg}">
        <spring:message code="errorInfo.error" /><c:out value="${errorMsg}" /><br/>
    </c:if>

    <c:if test="${not empty expire}">
            <spring:message code="activationForm.expire" /><br/>
    </c:if>

    <c:if test="${not empty failed}">
            <spring:message code="activationForm.failed" /><br/>
    </c:if>

    <form:form action="${submitActivationUrl}" modelAttribute="accountActivationForm">
   	    <form:errors path="*">
   			<spring:message code="error.global" />
   		</form:errors>

   		<br/>
   		<spring:message code="activationForm.label.email" />
        <form:input path="email" />
        <form:errors path="email" htmlEscape="false" />

        <br/>
        <spring:message code="activationForm.label.activationCode" />
        <form:input path="activationCode" />
    	<form:errors path="activationCode" htmlEscape="false" />
        <br/>

        <input type="submit" value="${register}"></input>
	</form:form>
</div>
</body>
</html>