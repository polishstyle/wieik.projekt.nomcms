package wieik.isi.nom.dto;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Sebastian on 2015-03-12.
 * Klasa bazowa dla klas encji zawierajaca ID encji.
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "{ id=" + id + '}';
    }
}
