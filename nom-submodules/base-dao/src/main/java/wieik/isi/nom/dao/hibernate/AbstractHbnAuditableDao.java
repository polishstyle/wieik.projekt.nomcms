package wieik.isi.nom.dao.hibernate;


import wieik.isi.nom.dao.AuditableDao;
import wieik.isi.nom.dto.AuditableEntity;

import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2015-03-12.
 */
public abstract class AbstractHbnAuditableDao<T extends AuditableEntity> extends AbstractHbnDao<T> implements AuditableDao<T> {

    @Override
    public long create(T t, String username) {
        if (username == null || username.length() == 0) {
            username = "ANONYMOUS_USER";
        }
        t.setCreated(new Date());
        t.setStatus("A");
        t.setCreatedBy(username);
        return create(t);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(long id) {
        return (T) getSession().createQuery("select x from " + getDomainClassName() + " x where x.id = " + id + " and x.status = \'A\'").uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        return getSession()
                .createQuery("select x from " + getDomainClassName() + " x where x.status = \'A\'")
                .list();
    }

    @Override
    public void update(T t, String username) {
        if (username == null || username.length() == 0) {
            username = "ANONYMOUS_USER";
        }
        t.setModified(new Date());
        t.setModifiedBy(username);
        update(t);
    }

    @Override
    public void delete(T t, String username) {
        if (username == null || username.length() == 0) {
            username = "ANONYMOUS_USER";
        }
        t.setModified(new Date());
        t.setModifiedBy(username);
        t.setStatus("D");
        getSession().update(t);
    }

    @Override
    public void deleteById(long id, String username) {
        T t = get(id);
        delete(t, username);
    }


    @Override
    public boolean existsActive(long id) {
        return (get(id) != null);
    }

    public void updateNotImpotantData(T t) {
        update(t);
    }
}
