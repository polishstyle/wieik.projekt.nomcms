package wieik.isi.nom.dao;


import wieik.isi.nom.dto.AuditableEntity;

import java.util.List;

/**
 * Created by Sebastian on 2015-03-12.
 */
public interface AuditableDao<T extends AuditableEntity> {
    long create(T t, String username);

    T get(long id);

    List<T> getAll();

    void update(T t, String username);

    void delete(T t, String username);

    void deleteById(long id, String username);

    long count();

    boolean exists(long id);

    boolean existsActive(long id);

    void updateNotImpotantData(T t);
}
