package wieik.isi.nom.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Sebastian on 2015-05-04.
 */
public abstract class BaseHbnDao {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private SessionFactory sessionFactory;
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
