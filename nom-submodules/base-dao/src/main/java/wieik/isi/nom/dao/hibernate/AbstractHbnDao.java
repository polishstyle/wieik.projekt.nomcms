package wieik.isi.nom.dao.hibernate;


import wieik.isi.nom.dao.Dao;
import wieik.isi.nom.dto.BaseEntity;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Sebastian on 2015-03-12.
 */
public abstract class AbstractHbnDao<T extends BaseEntity> extends BaseHbnDao implements Dao<T> {


    private Class<T> domainClass;



    @SuppressWarnings("unchecked")
    protected Class<T> getDomainClass() {
        if (domainClass == null) {
            ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
            this.domainClass = (Class<T>) thisType.getActualTypeArguments()[0];
        }
        return domainClass;
    }

    protected String getDomainClassName() {
        return getDomainClass().getName();
    }

    @Override
    public long create(T t) {
        Serializable s = getSession().save(t);
        //fixme: przydkie obejscie problemu
        try {
            Long l = (Long) s;
            return l;
        } catch (ClassCastException e) {
            return (Integer) s;
        }

    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(long id) {
        return (T) getSession().get(getDomainClass(), id);
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        return getSession()
                .createQuery("select x from " + getDomainClassName() + " x")
                .list();
    }

    @Override
    public void update(T t) {
        getSession().update(t);
    }

    @Override
    public void delete(T t) {
        getSession().delete(t);
    }

    @Override
    public void deleteById(long id) {
        T t = get(id);
        delete(t);
    }

    @Override
    public long count() {
        return (Long) getSession()
                .createQuery("select count(*) from " + getDomainClassName())
                .uniqueResult();
    }

    @Override
    public boolean exists(long id) {
        return (get(id) != null);
    }
}
