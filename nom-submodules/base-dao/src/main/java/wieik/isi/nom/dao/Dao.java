package wieik.isi.nom.dao;


import wieik.isi.nom.dto.BaseEntity;

import java.util.List;

/**
 * Created by Sebastian on 2015-03-12.
 */
public interface Dao<T extends BaseEntity> {
    long create(T t);

    T get(long id);

    List<T> getAll();

    void update(T t);

    void delete(T t);

    void deleteById(long id);

    long count();

    boolean exists(long id);
}
