package wieik.isi.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * Created by Sebastian on 2015-03-12.
 * Klasa bazowa dla audytowalnych encji.
 */
@MappedSuperclass
public abstract class AuditableEntity extends BaseEntity {
    private static final String STATUS_REGEXP = "^[AD]$";
    private static final String STATUS_DELETED = "D";
    private static final String STATUS_ACTIVE = "A";

    @NotNull
    @Pattern(regexp=STATUS_REGEXP, message="validation.wrong.status")
    @Column (name = "Status")
    protected String status;

    @NotNull
    @Column(name = "Created")
    protected Date created;

    @NotEmpty
    @Column(name = "CreatedBy")
    protected String createdBy;

    @Column(name = "Modified")
    protected Date modified;

    @Column(name = "ModifiedBy")
    protected String modifiedBy;

    @Column(name = "Version")
    protected int version;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isActive() {
        return status.equals(STATUS_ACTIVE);
    }

    public boolean isDeleted() {
        return status.equals(STATUS_DELETED);
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "{ status='" + status + '\'' +
                ", created=" + created +
                ", createdBy='" + createdBy + '\'' +
                ", modified=" + modified +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", version=" + version +
                "} " + super.toString();
    }
}
