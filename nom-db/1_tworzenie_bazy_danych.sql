-- Nalezy utworzyc uzytkownika nom_dev:nomDev123 i nadac mu prawa select, insert, update, delete

-- Nie wstawiamy dat z serwera bazy danych. Zawsze z aplikacji! --

-- Szablon dla tabel --
-- -- ABC --
-- create table Abcs (
	-- AbcID int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID abc.',
	
	-- Status varchar(1) not null comment 'Status rekordu: A-aktywny, D-usuniety.',
	-- Created timestamp not null comment 'Data utworzenia rekordu.',
	-- CreatedBy VARCHAR(64) not null comment 'Tworca rekordu.',
	-- Modified timestamp default 0 comment 'Data ostatniej modyfikacji rekordu.',
	-- ModifiedBy VARCHAR(64) comment 'Osoba ostatnio modyfikujaca rekord.',
	-- primary key (AbcID)
-- ) comment 'Abc.';


DROP DATABASE IF EXISTS NOM;
CREATE DATABASE NOM
	DEFAULT COLLATE = utf8_polish_ci;
use nom;

-- ---------- KONTA UZYTKOWNIKOW ---------- --
CREATE TABLE Accounts (
	AccountID INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID konta.',
	Username VARCHAR(32) NOT NULL UNIQUE COMMENT 'Unikalna nazwa uzytkownika.',
	Password VARCHAR(64) COMMENT 'Zaszyfrowane haslo.',
	Email VARCHAR(254) NOT NULL UNIQUE COMMENT 'Adres e-mail.',
	Sex VARCHAR(1) NOT NULL COMMENT 'Plec, M - mezczyzna, W - kobieta.',
    Enabled BIT(1) NOT NULL DEFAULT 1 COMMENT 'Czy konto jest aktywne.',
    Created TIMESTAMP NOT NULL COMMENT 'Data utworzenia.',
	ActivationCodeSent TIMESTAMP NOT NULL COMMENT 'Data wyslania kodu aktywacyjnego.',
	Question VARCHAR(254) COMMENT 'Pytanie do przypomnienia hasla.',
	Answer VARCHAR(64) COMMENT 'Zaszyfrowana odpowiedz.',
	PRIMARY KEY (AccountId)
) COMMENT 'Konta uzytkownikow.';
ALTER TABLE Accounts AUTO_INCREMENT=1001;


-- ---------- ROLE W SYSTEMIE ---------- --
CREATE TABLE Roles (
	RoleID INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID roli.',
	Name VARCHAR(16) NOT NULL UNIQUE COMMENT 'Unikalna nazwa roli.',
	Description VARCHAR(64) NOT NULL COMMENT 'Opis roli.',
	PRIMARY KEY (RoleID)
) COMMENT 'Role w systemie.';
ALTER TABLE Roles AUTO_INCREMENT=1001;

INSERT INTO Roles (RoleID, Name, Description) VALUES
(1, 'SYSTEM_ADMIN', 'Administrator systemu.'),
(2, 'USER_ADMIN', 'Administrator uzytkownikow.'),
(3, 'EDITOR_IN_CHIEF', 'Redaktor naczelny.'),
(4, 'MODERATOR', 'Moderator.'),
(5, 'ADVERTISER', 'Reklamodawca.'),
(6, 'USER', 'Uzytkownik.'),
(7, 'EDITOR', 'Redaktor.');

-- ---------- KONTO - ROLA ---------- --
create table AccountRole (
	AccountID int(11) not null comment 'ID konta.',
	RoleID int(11) not null comment 'ID roli.',
	constraint AccountRole_Account foreign key (AccountID) references Accounts (AccountID),
	constraint AccountRole_Role foreign key (RoleID) references Roles (RoleID)
) comment 'Powiazanie kont z rolami.';


-- ---------- SZABLONY ---------- --
create table Templates (
	TemplateID int(11) not null AUTO_INCREMENT comment 'ID szablonu.',
	Name VARCHAR(16) not null unique comment 'Unikalna nazwa szablonu.',
    Body text not null comment 'Cialo szablonu.',
	PRIMARY KEY (TemplateID)
) comment 'Szablony.';


-- ---------- SEKCJE ---------- --
create table Sections (
	SectionID int(11) not null AUTO_INCREMENT comment 'ID sekcji.',
	Name VARCHAR(16) not null unique comment 'Unikalna nazwa sekcji.',
    Priority tinyint(2) not null comment 'Priorytet sekcji.',
	TemplateID int(11) not null comment 'ID szablonu.',
	PRIMARY KEY (SectionID),
	constraint Sections_Template foreign key (TemplateID) references Templates (TemplateID)
) comment 'Sekcje.';


-- ---------- REDAKTORZY ---------- --
create table Editors (
	EditorID int(11) not null AUTO_INCREMENT comment 'ID redaktora.',
	FirstName VARCHAR(32) not null unique comment 'Unikalna nazwa sekcji.',
	LastName VARCHAR(32) not null unique comment 'Unikalna nazwa sekcji.',
	AccountID int(11) not null comment 'ID roli.',
	PRIMARY KEY (EditorID),
	constraint Editor_Account foreign key (AccountID) references Accounts (AccountID)
) comment 'Redaktorzy.';


-- ---------- SEKCJA - REDAKTOR NACZELNY ---------- --
create table SectionInChief (
	SectionID int(11) not null comment 'ID konta.',
	EditorID int(11) not null comment 'ID roli.',
	constraint SectionInChief_Section foreign key (SectionID) references Sections (SectionID),
	constraint SectionInChief_Editor foreign key (EditorID) references Editors (EditorID)
) comment 'Powiazanie sekcji z redaktorami naczelnymi.';


-- ---------- SEKCJA - REDAKTOR ---------- --
create table SectionEditor (
	SectionID int(11) not null comment 'ID konta.',
	EditorID int(11) not null comment 'ID roli.',
	constraint SectionEditor_Section foreign key (SectionID) references Sections (SectionID),
	constraint SectionEditor_Editor foreign key (EditorID) references Editors (EditorID)
) comment 'Powiazanie sekcji z redaktorami.';


-- ---------- SEKCJA - MODERATOR ---------- --
create table SectionModerator (
	SectionID int(11) not null comment 'ID konta.',
	EditorID int(11) not null comment 'ID roli.',
	constraint SectionModerator_Section foreign key (SectionID) references Sections (SectionID),
	constraint SectionModerator_Editor foreign key (EditorID) references Editors (EditorID)
) comment 'Powiazanie sekcji z moderatorami.';


-- ---------- ARTYKULY ---------- --
create table Articles (
	ArticleID int(11) not null AUTO_INCREMENT comment 'ID artykulu.',
	Status varchar(1) not null comment 'Status rekordu: A-aktywny, D-usuniety.',
    Created timestamp not null comment 'Data utworzenia rekordu.',
    CreatedBy VARCHAR(64) not null comment 'Tworca rekordu.',
    Modified timestamp default 0 comment 'Data ostatniej modyfikacji rekordu.',
    ModifiedBy VARCHAR(64) comment 'Osoba ostatnio modyfikujaca rekord.',
	Name VARCHAR(64) not null unique comment 'Unikalna nazwa artykulu - tylko litery i cyfry.',
	Title VARCHAR(64) not null comment 'Tytul artykulu.',
    Content text not null comment 'Tresc artykulu.',
    Priority tinyint(1) not null comment 'Priorytet.',
    PublicationDate timestamp comment 'Data publikacji.',
    State varchar(1) not null comment 'Stan artykulu: P - opublikowany, E - edycja, C - sprawdzany.',
	SectionID int(11) not null comment 'ID sekcji.',
	EditorID int(11) not null comment 'ID autora.',
	TemplateID int(11) not null comment 'ID szablonu.',
	VotesYes int(8) not null default 0 comment 'Glosy na tak.',
	VotesNo int(8) not null default 0 comment 'Glosy na nie.',
	Version int(4) not null default 0 comment 'Wersja.',
	Preview VARCHAR(255) not null comment 'Skrot artykulu.',
	ImageName VARCHAR(64) comment 'Nazwa grafiki do artykulu.',
	PRIMARY KEY (ArticleID),
	constraint Article_Section foreign key (SectionID) references Sections (SectionID),
	constraint Article_Author foreign key (EditorID) references Editors (EditorID),
	constraint Article_Template foreign key (TemplateID) references Templates (TemplateID)
) comment 'Artykuly.';


-- ---------- KOMENTARZE ---------- --
create table Comments (
	CommentID int(11) not null AUTO_INCREMENT comment 'ID komentarza.',
	Content VARCHAR(32) not null comment 'Zawartosc komentarza.',
	CreationDate timestamp not null comment 'Data utworzenia komentarza.',
	AccountID int(11) not null comment 'ID konta.',
	ArticleID int(11) not null comment 'ID artykulu.',
	PRIMARY KEY (CommentID),
	constraint Comment_Account foreign key (AccountID) references Accounts (AccountID),
	constraint Comment_Article foreign key (ArticleID) references Articles (ArticleID)
) comment 'Komentarze.';


-- ---------- GLOSY ---------- --
create table Votes (
	VoteID int(11) not null AUTO_INCREMENT comment 'ID glosu.',
	Ok tinyint(1) not null comment 'Czy glos na tak.',
	AccountID int(11) not null comment 'ID konta.',
	ArticleID int(11) not null comment 'ID artykulu.',
	PRIMARY KEY (VoteID),
	constraint Votes_Account foreign key (AccountID) references Accounts (AccountID),
	constraint Votes_Article foreign key (ArticleID) references Articles (ArticleID)
) comment 'Glosy.';


-- ---------- WULGARYZMY ---------- --
create table Vulgarisms (
	Vulgarism varchar(32) not null unique comment 'Wulgaryzm'
) comment 'Wulgaryzmy.';

