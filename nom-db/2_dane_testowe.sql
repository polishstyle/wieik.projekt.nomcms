use nom;

-- DANE KONT --
-- HASLA - Imie124 --
INSERT INTO `accounts` (`AccountID`,`Username`,`Password`,`Email`,`Sex`,`Enabled`,`Created`,`ActivationCodeSent`,`Question`,`Answer`) VALUES (1001,'SMMila','$2a$10$eYMPbMB7wv8oSp.i7P8CqOIgFkTXXOZlSZ3b8nXpyH5eIwE/qYGPG','seba.mila@gmail.com','M',1,'2015-06-03 23:09:45','2015-06-03 23:09:45','Kot Natalii?',NULL);
INSERT INTO `accounts` (`AccountID`,`Username`,`Password`,`Email`,`Sex`,`Enabled`,`Created`,`ActivationCodeSent`,`Question`,`Answer`) VALUES (1002,'DNowak','$2a$10$TsjqfDrBZ5xj3pDSENNyYOymJEjELllMjKJzwtcvdInKw1ZTXYzAe','nowakfraniu@gmail.com','M',1,'2015-06-03 23:11:03','2015-06-03 23:11:03','Jak masz na imię?',NULL);
INSERT INTO `accounts` (`AccountID`,`Username`,`Password`,`Email`,`Sex`,`Enabled`,`Created`,`ActivationCodeSent`,`Question`,`Answer`) VALUES (1004,'AOlkiewicz','$2a$10$ivUgdGsec1qE0t1TyFQwO.h3/9zx9S4n92S5.H5p2vWsPM4YRxzQu','aolkiewicz@gmail.com','M',1,'2015-06-03 23:12:38','2015-06-03 23:12:38','Jak masz na imię??',NULL);

INSERT INTO Templates (TemplateID, Name, Body) VALUES
(1, 'standard', 'div.subhead{background:#808080;}div.content{float:left;width:70%;}div.rightMenu{float:right;width:30%;background:#C0C0C0;}div.footer{clear:both;height:10%; background:#808080;}'),
(2, 'inny', 'div.subhead{backgroud:#396;} div.content{float:left;width:70%;} div.rightMenu{float:right;width:30%;} div.footer{clear:both; height:10%; backgroud:#396;}');

INSERT INTO Sections (SectionID, Name, Priority, TemplateID) VALUES
(1, 'Kraj', 5, 1),
(2, 'Europa', 4, 1),
(3, 'Świat', 3, 1),
(4, 'Sport', 2, 2),
(5, 'Kultura', 1, 2);

INSERT INTO AccountRole (AccountID, RoleID) VALUES
(1001, 1),(1001, 2),(1001, 3),(1001, 6),(1001, 7),
(1002, 1),(1002, 4),(1002, 3),(1002, 6),(1002, 7),
(1004, 1),(1004, 5),(1004, 3),(1004, 6),(1004, 7);

INSERT INTO Editors (EditorID, FirstName, LastName, AccountID) VALUES
(1, 'Sebastian', 'Mila', 1001),
(2, 'Damian', 'Nowak', 1002),
(3, 'Andrzej', 'Olkiewicz', 1004);

INSERT INTO SectionInChief (SectionID, EditorID) VALUES
(1,1),(1,2),
(2,1),(2,3),
(3,1),(3,3),
(4,2),(4,3),
(5,1),(5,2);

INSERT INTO SectionEditor (SectionID, EditorID) VALUES
(1,1),(1,2),(1,3),
(2,1),(2,2),(2,3),
(3,1),(3,2),(3,3),
(4,2),(4,3),
(5,1),(5,2);

INSERT INTO SectionModerator (SectionID, EditorID) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2);

INSERT INTO Vulgarisms (Vulgarism) VALUES
('kurwa'),('huj'),('spierdalaj'),('chuj'),('bul');



INSERT INTO Articles (ArticleID, Status, Created, CreatedBy, Modified, ModifiedBy, Name, Title, Content, Priority, PublicationDate, State, SectionID, EditorID, TemplateID, VotesYes, VotesNo, Version, Preview, ImageName) VALUES
(1, 'A', '2015-06-03 23:09:45', 'ADMIN', null, null, 'art1', 'NOM-Info startuje', 'Wlasnie wystartowal serwis informacyjny NOM-Info!', 1, '2015-06-03 23:09:45', 'P', 1, 1, 1, 1, 0, 1, 'Wlasnie wystartowal serwis informacyjny NOM-Info!', 'a1.png'),
(2, 'A', '2015-06-03 23:09:45', 'ADMIN', null, null, 'NOM-Info_startuje_2', 'NOM-Info startuje 2', 'Wlasnie wystartowal serwis informacyjny NOM-Info!1111 11', 2, '2015-06-03 23:09:45', 'C', 1, 1, 1, 0, 0, 1, 'Wlasnie wystartowal serwis informacyjny NOM-Info!', 'a1.png'),
(3, 'A', '2015-06-03 23:09:45', 'ADMIN', null, null, 'NOM-Info_startuje_3', 'NOM-Info startuje 3', 'Wlasnie wystartowal serwis informacyjny NOM-Info!12 22222', 3, '2015-06-03 23:09:45', 'E', 1, 1, 1, 0, 0, 1, 'Wlasnie wystartowal serwis informacyjny NOM-Info!', 'a1.png'),
(4, 'A', '2015-06-03 23:09:45', 'ADMIN', null, null, 'Derby_Krakowa', 'Derby Krakowa', 'Legia przegrala z LZS Kręcina Wieliczka', 1, '2015-06-03 23:09:45', 'P', 4, 2, 1, 0, 0, 1, 'Porazka Legii!11', 'a1.png');

INSERT INTO Votes (VoteID, Ok, AccountID, ArticleID) VALUES
(1, 1, 1001, 1);

INSERT INTO Comments (CommentID, Content, CreationDate, AccountID, ArticleID) VALUES
(1, 'Pierfszy !!11!', '2015-06-03 23:09:50', 1001, 1);





