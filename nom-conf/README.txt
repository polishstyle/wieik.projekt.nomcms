Instrukcja opisuje jak uruchomi� i testowa� lokalnie NOM-CMS.

I. WYMAGANIA
1. MySQL Server 5.6
2. JDK 8
3. Apache Maven 3.2.3
4. Apache Tomcat 8
    4.1 Bilblioteki do Tomcata (katalog lib w Tomcacie):
        a) activation-1.1.1.jar
        b) mysql-connector-java-5.1.27.jar
        c) mail-1.4.7.jar

II. SRODOWISKO PROGRAMISTYCZNE (zalecane)
1. IDEA IntelliJ
2. MySQL Workbench

III. KONFIGURACJA
1. Tomcat wg. pliku tomcat.txt
2. Instalacja bazy wg plików w katalogu nom-db

IV. BUDOWANIE PROJEKTU
1. Okno poleń (lub przez terminal w IntelliJ) i polecenie mvn clean package

V. URUCHAMIANIE
1. Paczke NOM.war (nom-webapp/target) wrzucamy do katalogu webapp tomcata.
2. Uruchamiamy Tomcata

