package wieik.nom.mail;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Sebastian on 2015-03-23.
 */
public class ActivationMailData {
    private long id;
    private String toEmail;
    private String activationCode;
    private Date sendDate;

    public ActivationMailData(long id, @NotEmpty String toEmail, @NotEmpty String activationCode, @NotNull Date sendDate) {
        this.id = id;
        this.toEmail = toEmail;
        this.activationCode = activationCode;
        this.sendDate = sendDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
}
