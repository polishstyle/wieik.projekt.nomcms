package wieik.nom.mail;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sebastian on 2015-03-24.
 */
@Component
public class NomMailSender {

    private class EmailData {
        String text;
        String subject;
        String toEmail;
        Date sentDate;

        public EmailData(String text, String subject, String toEmail, Date sentDate) {
            this.text = text;
            this.subject = subject;
            this.toEmail = toEmail;
            this.sentDate = sentDate;
        }
    }

    @Value("#{accountServiceProps.fromEmailAddress}")
    private String fromEmailAddress;

    @Value("#{accountServiceProps.activationUrl}")
    private String activationUrl;

    @Value("#{accountServiceProps.activationPageUrl}")
    private String activationPageUrl;

    @Value("#{accountServiceProps.activationMailTemplate}")
    private String activationMailTemplate;

    @Value("#{accountServiceProps.portalName}")
    private String portalName;

    @Value("#{accountServiceProps.activationMailSubject}")
    private String activationMailSubject;

    @Value("#{accountServiceProps.newPasswordMailSubject}")
    private String newPasswordMailSubject;

    @Value("#{accountServiceProps.newPasswordMailTemplate}")
    private String newPasswordMailTemplate;

    @Inject
    private VelocityEngine velocityEngine;

    @Inject
    private JavaMailSender javaMailSender;

    @Async
    public void sendActivationEmail(ActivationMailData activationMailData) {
        MimeMessage mimeMessage = createActivationEmail(activationMailData);
        sendEmail(mimeMessage);
    }

    @Async
    public void sendNewPassword(String email, String newPassword) {
        MimeMessage mimeMessage = createNewPasswordEmail(email, newPassword);
        sendEmail(mimeMessage);
    }

    private MimeMessage createActivationEmail(ActivationMailData activationMailData) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("portalName", portalName);
        model.put("activationLink", getActivationUrl(activationMailData));
        model.put("activationCode", activationMailData.getActivationCode());
        model.put("activationPage", activationPageUrl);

        String text = buildEmailText(activationMailTemplate, model);
        EmailData emailData = new EmailData(text, activationMailSubject, activationMailData.getToEmail(), activationMailData.getSendDate());
        return buildMimeMessage(emailData);
    }

    private String buildEmailText(String velocityTemplate, Map<String, Object> model) {
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, activationMailTemplate, model);
        text = text.replaceAll("\n", "<br>");
        return text;
    }

    private MimeMessage buildMimeMessage(EmailData emailData) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setSubject(emailData.subject);
            helper.setTo(emailData.toEmail);
            helper.setFrom(fromEmailAddress);
            helper.setSentDate(emailData.sentDate);
            helper.setText(emailData.text, true);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        return mimeMessage;
    }


    private String getActivationUrl(ActivationMailData activationMailData) {
        return activationUrl + "?a=" + activationMailData.getId() + "&d=" + activationMailData.getActivationCode();
    }

    private MimeMessage createNewPasswordEmail(String email, String newPassword) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("portalName", portalName);
        model.put("newPassword", newPassword);

        String text = buildEmailText(activationMailTemplate, model);
        EmailData emailData = new EmailData(text, activationMailSubject, email, new Date());
        return buildMimeMessage(emailData);
    }

    private void sendEmail(MimeMessage mimeMessage) {
        javaMailSender.send(mimeMessage);
    }
}
