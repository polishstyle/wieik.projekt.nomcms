package wieik.nom.dao;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface VulgarismDao {
    void add(String vulgarism);

    void delete(String vulgarism);

    boolean exists(String vulgarism);

    List<String> getAll();
}
