package wieik.nom.dao;

import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Vote;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface VoteDao extends Dao<Vote> {
    void createVote(long articleId, int ok, long accountId);
}
