package wieik.nom.dao;

import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Section;
import wieik.nom.vo.LinkVO;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface SectionDao extends Dao<Section> {
    Section findByName(String sectionName);

    List<LinkVO> getLinksWithPriority(Section section, int priority, int n);

    List<LinkVO> getLinks(Section section, int page, int limit);

    int coutArticles(Section section);

    List<LinkVO> getTopLinks(Section s, int n);

    List<Section> getSectionList();

    void addSection(String sectionName, String sectionPriority, String sectionTemplate);
}
