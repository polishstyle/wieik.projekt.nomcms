package wieik.nom.dao;

import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Article;
import wieik.nom.dto.Comment;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface CommentDao extends Dao<Comment> {
    List<Comment> getForArticle(Article article, int page, int limit);

    int countForArticle(long articleId);

    void deleteComment(String content);
}
