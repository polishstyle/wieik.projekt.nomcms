package wieik.nom.dao.hibernate;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.SectionDao;
import wieik.nom.dto.Role;
import wieik.nom.dto.Section;
import wieik.nom.vo.LinkVO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class SectionHbnDao extends AbstractHbnDao<Section> implements SectionDao {

    private final static String SELECT_LINKVO = "select Title, Preview, Name, Priority, SectionID, ImageName from Articles where SectionID = ? and State = 'P' and Status = 'A' order by ArticleID desc limit ?, ?";
    private final static String SELECT_N_LINKVO_WITH_PRIORITY = "select Title, Preview, Name, Priority, SectionID, ImageName from Articles where SectionID = ? and State = 'P' and Status = 'A' and Priority = ? order by ArticleID desc limit ?";
    private final static String SELECT_TOP_LINKVO = "select Title, Preview, Name, Priority, SectionID, ImageName from Articles where SectionID = ? and State = 'P' and Status = 'A' order by Priority, ArticleID desc limit ?";


    static final class LinkVOMapper implements RowMapper<LinkVO> {

        public LinkVO mapRow(ResultSet rs, int rowNum) throws SQLException {
            LinkVO linkVO = new LinkVO(rs.getString("Title"), rs.getString("Preview"), rs.getString("Name"), rs.getInt("Priority"), rs.getInt("SectionID"), rs.getString("ImageName"));
            return linkVO;
        }
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public Section findByName(String sectionName) {
        Query q = getSession().getNamedQuery("findSectionByName");
        q.setParameter("sectionName", sectionName);
        return (Section) q.uniqueResult();
    }

    public List<LinkVO> getLinksWithPriority(Section section, int priority, int n) {
        List<LinkVO> list = this.jdbcTemplate.query(SELECT_N_LINKVO_WITH_PRIORITY, new LinkVOMapper(), section.getId(), priority, n);
        for (LinkVO e : list) {
            e.setLink("/NOM/p/" + section.getName() + '/' + e.getLink());
        }
        return list;
    }

    public List<LinkVO> getLinks(Section section, int page, int limit) {
        List<LinkVO> list = this.jdbcTemplate.query(SELECT_LINKVO, new LinkVOMapper(), section.getId(), firstResult(limit, page), limit);
        for (LinkVO e : list) {
            e.setLink("/NOM/p/" + section.getName() + '/' + e.getLink());
        }
        return list;
    }

    public int coutArticles(Section section) {
        Query q = getSession().getNamedQuery("countArticlesForArticle");
        q.setParameter("sectionId", section.getId());
        Long l = (Long) q.uniqueResult();
        if (l != null) {
            return l.intValue();
        }
        return 0;
    }

    private int firstResult(int limit, int page) {
        int firstResult = 0;
        if (page > 1) {
            firstResult = (limit * page) - limit;
        }
        return firstResult;
    }

    public List<LinkVO> getTopLinks(Section section, int n) {
        List<LinkVO> list = this.jdbcTemplate.query(SELECT_TOP_LINKVO, new LinkVOMapper(), section.getId(), n);
        for (LinkVO e : list) {
            e.setLink("/NOM/p/" + section.getName() + '/' + e.getLink());
        }
        return list;
    }

    @Override
    public List<Section> getSectionList() {

        String sql = "SELECT * FROM nom.sections";

        List<Section> sectionList = new ArrayList<Section>();

        sectionList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Section>(Section.class));

        return sectionList;
    }

    @Override
    public void addSection(String sectionName, String sectionPriority, String sectionTemplate) {
        String sql = "INSERT INTO nom.sections (Name, Priority, TemplateID) VALUES (?,?,?)";

        jdbcTemplate.update(sql, new Object[] {sectionName, sectionPriority, sectionTemplate});
    }
}
