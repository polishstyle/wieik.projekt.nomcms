package wieik.nom.dao.hibernate;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.CommentDao;
import wieik.nom.dto.Article;
import wieik.nom.dto.Comment;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class CommentHbnDao extends AbstractHbnDao<Comment> implements CommentDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Comment> getForArticle(Article article, int page, int limit) {
        Query q = getSession().getNamedQuery("getCommentsForArticle");
        int firstResult = 0;
        if (page > 1) {
            firstResult = (limit * page) - limit;
        }
        q.setFirstResult(firstResult);
        q.setMaxResults(limit);
        q.setParameter("articleId", article.getId());
        return (List<Comment>) q.list();
    }

    public int countForArticle(long articleId) {
        Query q = getSession().getNamedQuery("countCommentsForArticle");
        q.setParameter("articleId", articleId);
        Long l = (Long) q.uniqueResult();
        if (l != null) {
            return l.intValue();
        }
        return 0;
    }

    @Override
    public void deleteComment(String content)
    {
        String sql = "DELETE FROM `nom`.`comments` WHERE `Content`= ?";

        jdbcTemplate.update(sql, new Object[]{content});
    }
}
