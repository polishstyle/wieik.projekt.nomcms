package wieik.nom.dao.hibernate;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.RoleDao;
import wieik.nom.dto.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebastian on 2015-03-17.
 */
public class RoleHbnDao extends AbstractHbnDao<Role> implements RoleDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Role getByName(String name) {
        Query q = getSession().getNamedQuery("findRoleByName");
        q.setParameter("name", name);
        return (Role) q.uniqueResult();
    }

    @Override
    public List<Role> getRolesForUser(String userName) {

        String sql = "SELECT r.Name FROM nom.roles r where r.RoleID IN (SELECT ar.RoleID FROM nom.accountrole ar where AccountID = (SELECT AccountID FROM nom.accounts where username = ?))";

        List<Role> rolesList = new ArrayList<Role>();

        rolesList = jdbcTemplate.query(sql, new Object[]{ userName}, new BeanPropertyRowMapper<Role>(Role.class));

        return rolesList;
    }

    @Override
    public List<Role> getAllRoles() {
        String sql = "SELECT r.Name FROM nom.roles r";

        List<Role> rolesList = new ArrayList<Role>();

        rolesList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Role>(Role.class));

        return rolesList;
    }

    @Override
    public void removeRoleForUser(String userName, String roleName) {
        String sql = "DELETE FROM nom.accountrole WHERE AccountID = (SELECT AccountID FROM nom.accounts where username = ?) AND RoleID = (SELECT RoleID FROM nom.roles WHERE Name = ?)";

        jdbcTemplate.update(sql, new Object[]{userName, roleName});
    }

    @Override
    public void addRoleForUser(String userName, String roleName) {
        String sql = "INSERT INTO nom.accountrole (AccountID, RoleID) VALUES ((SELECT AccountID FROM nom.accounts where username = ?), (SELECT RoleID FROM nom.roles WHERE Name = ?))";

        jdbcTemplate.update(sql, new Object[]{userName, roleName});
    }


}
