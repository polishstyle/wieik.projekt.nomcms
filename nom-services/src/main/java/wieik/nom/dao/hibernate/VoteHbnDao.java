package wieik.nom.dao.hibernate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.VoteDao;
import wieik.nom.dto.Vote;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class VoteHbnDao extends AbstractHbnDao<Vote> implements VoteDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void createVote(long articleId, int ok, long accountId) {
        jdbcTemplate.update("insert into Votes (ArticleID, Ok, AccountID) values (?, ?, ?)", articleId, ok, accountId);
    }
}
