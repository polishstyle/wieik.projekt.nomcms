package wieik.nom.dao.hibernate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.AccountDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Role;
import wieik.nom.dto.UserProfile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2015-02-11.
 */
public class AccountHbnDao extends AbstractHbnDao<Account> implements AccountDao {
    private static final String UPDATE_PASSWORD_SQL =
            "update Accounts set password = ? where username = ?";
    private static final String SELECT_PASSWORD_SQL =
            "select password from Accounts where AccountID = ?";
    private static final String UPDATE_ANSWER_SQL =
            "update Accounts set answer = ? where email = ?";
    private static final String SELECT_ANSWER_SQL =
            "select Answer from Accounts where AccountID = ?";

    private static final Logger logger = LoggerFactory.getLogger(AccountHbnDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public long create(Account account, String password, String answer) {
        long id = create(account);
        String encPassword = passwordEncoder.encode(password);
        String encAnswer = passwordEncoder.encode(answer);
        jdbcTemplate.update(UPDATE_PASSWORD_SQL, encPassword, account.getUsername());
        jdbcTemplate.update(UPDATE_ANSWER_SQL, encAnswer, account.getUsername());
        return id;
    }

    public Account findByUsername(String username) {
        Query q = getSession().getNamedQuery("findAccountByUsername");
        q.setParameter("username", username);
        return (Account) q.uniqueResult();
    }

    public String getEncodedPassword(Account account) {
        return jdbcTemplate.queryForObject(SELECT_PASSWORD_SQL, String.class, account.getId());
    }

    public Account findByEmail(String email) {
        Query q = getSession().getNamedQuery("findAccountByEmail");
        q.setParameter("email", email);
        return (Account) q.uniqueResult();
    }

    public Date getActivationCodeSentDate(long id) {
        Query q = getSession().getNamedQuery("getActivationCodeSentDate");
        q.setParameter("id", id);
        return (Date) q.uniqueResult();
    }

    public void changePassword(Account account, String newPassword) {
        String encPassword = passwordEncoder.encode(newPassword);
        jdbcTemplate.update(UPDATE_PASSWORD_SQL, encPassword, account.getUsername());
    }

    public boolean checkAnswer(Account account, String answer) {
        String encPassword = jdbcTemplate.queryForObject(SELECT_ANSWER_SQL, String.class, account.getId());
        return passwordEncoder.matches(answer, encPassword);
    }

    public boolean checkPassword(Account account, String oldPassword) {
        String encPassword = jdbcTemplate.queryForObject(SELECT_PASSWORD_SQL, String.class, account.getId());
        return passwordEncoder.matches(oldPassword, encPassword);
    }

    public List<Account> listAccount(){
        List<Account> accounts = new ArrayList<Account>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM Accounts");

        for(Map row: rows)
        {
            Account account = new Account();
            account.setUsername((String)row.get("Username"));
            account.setEmail((String) row.get("Email"));
            account.setSex((String) row.get("Sex"));

            accounts.add(account);
        }

        return accounts;
    }

    @Override
    public void BlockUser(String userName) {
        String sql = "UPDATE nom.accounts SET Enabled=0 WHERE AccountID IN (SELECT AccountID FROM( SELECT AccountID FROM nom.accounts where nom.accounts.Username = ? ) as t)";

        jdbcTemplate.update(sql, new Object[]{userName});
    }

    @Override
    public void DeleteUser(String userName) {
        String sql = "DELETE FROM nom.accountrole WHERE AccountID IN (SELECT AccountID FROM( SELECT AccountID FROM nom.accounts where nom.accounts.Username = ? ) as t)";

        jdbcTemplate.update(sql, new Object[]{userName});

        String sql2 = "DELETE FROM nom.accounts WHERE AccountID IN (SELECT AccountID FROM( SELECT AccountID FROM nom.accounts where nom.accounts.Username = ? ) as t)";

        jdbcTemplate.update(sql2, new Object[]{userName});
    }

    @Override
    public void UpdateAccountProfile(long AccountID, String name, String surname, int Age, String hobby) {
        String sql = "INSERT INTO nom.accountprofile (AccountID, Name, Surname, Age, Hobby) VALUES (?, ?, ?, ?, ?)";
        String sql2 = "UPDATE nom.accountprofile SET Name = ?, Surname = ?, Age = ?, Hobby = ? WHERE AccountID = ?";

        Integer cnt = jdbcTemplate.queryForObject(
                "SELECT count(*) FROM nom.accountprofile WHERE AccountID = ?", new Object[]{AccountID}, Integer.class);

        if(cnt != null && cnt > 0)
        {
            jdbcTemplate.update(sql2, new Object[]{name, surname, Age, hobby, AccountID});
        } else {
            jdbcTemplate.update(sql, new Object[]{AccountID, name, surname, Age, hobby});
        }
    }

    @Override
    public Account getAccountID(String userName) {
        String sql = "SELECT AccountID FROM nom.accounts WHERE Username = ?";

        Integer accountID = (Integer) jdbcTemplate.queryForObject(sql, new Object[]{userName}, Integer.class);

        Account account = new Account();
        account.setId(accountID);

        return account;
    }

    @Override
    public UserProfile getUserProfile(String userName) {
        String sql = "SELECT * FROM nom.accountprofile WHERE AccountID = ?";

        Account account = getAccountID(userName);

        UserProfile userProfile = jdbcTemplate.queryForObject(sql, new Object[]{account.getId()}, new BeanPropertyRowMapper<UserProfile>(UserProfile.class));
        return userProfile;
    }
}
