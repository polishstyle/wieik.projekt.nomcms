package wieik.nom.dao.hibernate;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.EditorDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Editor;
import wieik.nom.dto.Section;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class EditorHbnDao extends AbstractHbnDao<Editor> implements EditorDao {
    private final static String COUNT_INCHIEF = "select count(EditorID) from SectionInChief where SectionID = ? and EditorID = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public Editor findByAccount(Account account) {
        Query q = getSession().getNamedQuery("findEditorByAccount");
        q.setParameter("accountId", account.getId());
        return (Editor) q.uniqueResult();
    }

    public boolean isInChief(Section section, Editor editor) {
        int i = jdbcTemplate.queryForInt(COUNT_INCHIEF, section.getId(), editor.getId());
        return i>0;
    }
}
