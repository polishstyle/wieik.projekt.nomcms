package wieik.nom.dao.hibernate;

import org.hibernate.Query;
import wieik.isi.nom.dao.hibernate.AbstractHbnDao;
import wieik.nom.dao.ThemeDao;
import wieik.nom.dto.Theme;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class ThemeHbnDao extends AbstractHbnDao<Theme> implements ThemeDao {
    private static final String DEFAULT_THEME_NAME = "standard";

    @Override
    public Theme getByName(String name) {
        Query q = getSession().getNamedQuery("findThemeByName");
        q.setParameter("themeName", name);
        return (Theme) q.uniqueResult();
    }

    @Override
    public Theme getDefault() {
        Query q = getSession().getNamedQuery("findThemeByName");
        q.setParameter("themeName", DEFAULT_THEME_NAME);
        return (Theme) q.uniqueResult();
    }
}
