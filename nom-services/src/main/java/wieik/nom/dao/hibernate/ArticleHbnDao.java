package wieik.nom.dao.hibernate;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import wieik.isi.nom.dao.hibernate.AbstractHbnAuditableDao;
import wieik.nom.dao.ArticleDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Article;
import wieik.nom.dto.Section;
import wieik.nom.dto.Vote;
import wieik.nom.vo.LinkVO;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class ArticleHbnDao extends AbstractHbnAuditableDao<Article> implements ArticleDao {

    private static final String SELECT_N_TOP_LINKVO = "select Title, Preview, Name, Priority, SectionID from Articles where and State = 'P' and Status = 'A' and Priority = ? order by Priority, PublicationDate desc limit ?";
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Article> getNewest(Section section, int limit) {
        Query q = getSession().getNamedQuery("getNewestArticlesForSection");
        q.setMaxResults(limit);
        q.setParameter("sectionId", section.getId());
        return (List<Article>) q.list();
    }

    @Transactional
    public List<Article> getForSection(Section section, int page, int limit) {
        Query q = getSession().getNamedQuery("getArticlesForSection");
        int firstResult = 0;
        if (page > 1) {
            firstResult = (limit * page) - limit;
        }
        q.setFirstResult(firstResult);
        q.setMaxResults(limit);
        q.setParameter("sectionId", section.getId());
        return (List<Article>) q.list();
    }

    public List<Article> getNewestWithPriority(int priority, int limit) {
        Query q = getSession().getNamedQuery("getNewestArticlesWithPriority");
        q.setMaxResults(limit);
        q.setParameter("priority", priority);
        return (List<Article>) q.list();
    }

    public List<Article> getInEdition(Section section, int page, int limit) {
        Query q = getSession().getNamedQuery("getArticlesInEditionForSection");
        int firstResult = 0;
        if (page > 1) {
            firstResult = (limit * page) - limit;
        }
        q.setFirstResult(firstResult);
        q.setMaxResults(limit);
        q.setParameter("sectionId", section.getId());
        return (List<Article>) q.list();
    }

    @Transactional
    public List<Article> getInCheck(Section section, int page, int limit) {
        Query q = getSession().getNamedQuery("getArticlesInCheckForSection");
        int firstResult = 0;
        if (page > 1) {
            firstResult = (limit * page) - limit;
        }
        q.setFirstResult(firstResult);
        q.setMaxResults(limit);
        q.setParameter("sectionId", section.getId());
        return (List<Article>) q.list();
    }


    public Article findByName(String articleName) {
        Query q = getSession().getNamedQuery("findArticleByName");
        q.setParameter("articleName", articleName);
        return (Article) q.uniqueResult();
    }

    public boolean wasVoting(Account account, Article article) {
        Query q = getSession().getNamedQuery("findVote");
        q.setParameter("voterId", account.getId());
        q.setParameter("articleId", article.getId());
        Vote v = (Vote) q.uniqueResult();
        return (v != null);
    }

    public void increaseVotesYes(long id, int i) {
        jdbcTemplate.update("update Articles set VotesYes = ? where ArticleId = ?", i, id);
    }

    public void increaseVotesNo(long id, int i) {
        jdbcTemplate.update("update Articles set VotesNo = ? where ArticleId = ?", i, id);
    }

    public List<LinkVO> getTopLinks(int n) {
        List<LinkVO> list = jdbcTemplate.query(SELECT_N_TOP_LINKVO, new SectionHbnDao.LinkVOMapper(), n);
        return list;
    }

    @Override
    public void setAcceptArticleInCheck(String ArticleName) {
        String sql = "UPDATE nom.articles SET State='P' WHERE Name=?";

        jdbcTemplate.update(sql, new Object[] {ArticleName});
    }

    @Override
    public void setArticlePriority(String ArticleName, String newPriority) {
        String sql = "UPDATE nom.articles SET Priority=? WHERE Name=?";

        jdbcTemplate.update(sql, new Object[] {newPriority, ArticleName});
    }

    @Override
    public void deleteArticle(String ArticleName) {
        String sql = "DELETE FROM nom.comments WHERE ArticleID = (SELECT ArticleID FROM nom.articles WHERE Name = ?)";

        jdbcTemplate.update(sql, new Object[] {ArticleName});

        String sql2 = "DELETE FROM nom.votes WHERE ArticleID = (SELECT ArticleID FROM nom.articles WHERE Name = ?)";

        jdbcTemplate.update(sql2, new Object[] {ArticleName});

        String sql3 = "DELETE FROM nom.articles WHERE Name=?";

        jdbcTemplate.update(sql3, new Object[] {ArticleName});
    }
}
