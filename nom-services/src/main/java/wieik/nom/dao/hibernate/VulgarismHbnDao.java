package wieik.nom.dao.hibernate;

import org.hibernate.SQLQuery;
import wieik.isi.nom.dao.hibernate.BaseHbnDao;
import wieik.nom.dao.VulgarismDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public class VulgarismHbnDao extends BaseHbnDao implements VulgarismDao {

    public void add(String vulgarism) {
        String sql = "insert into Vulgarisms (Vulgarism) values (?)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter(0, vulgarism);
        query.executeUpdate();
    }

    public void delete(String vulgarism) {
        String sql = "delete from Vulgarisms where Vulgarism = ?";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter(0, vulgarism);
        query.executeUpdate();
    }

    public boolean exists(String vulgarism) {
        String sql = "select count(vulgarism) from Vulgarisms where Vulgarism = ?";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter(0, vulgarism);
        int a = (Integer) query.uniqueResult();
        if (a > 0)
            return true;
        return false;
    }

    public List<String> getAll() {
        String sql = "select vulgarism from Vulgarisms";
        SQLQuery query = getSession().createSQLQuery(sql);
        return (List<String>) query.list();
    }
}
