package wieik.nom.dao;

import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Role;

import java.util.List;

/**
 * Created by Sebastian on 2015-03-17.
 */
public interface RoleDao extends Dao<Role> {
    Role getByName(String name);
    List<Role> getRolesForUser(String userName);
    List<Role> getAllRoles();
    void removeRoleForUser(String userName, String roleName);
    void addRoleForUser(String userName, String roleName);
}
