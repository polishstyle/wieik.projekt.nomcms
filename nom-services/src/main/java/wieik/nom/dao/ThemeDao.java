package wieik.nom.dao;

import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Theme;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface ThemeDao extends Dao<Theme> {
    Theme getByName(String name);

    Theme getDefault();
}
