package wieik.nom.dao;


import org.springframework.transaction.annotation.Transactional;
import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Account;
import wieik.nom.dto.UserProfile;

import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2015-02-11.
 */
public interface AccountDao extends Dao<Account> {
    long create(Account account, String password, String answer);

    Account findByUsername(String email);

    String getEncodedPassword(Account account);

    Account findByEmail(String email);

    Date getActivationCodeSentDate(long id);

    void changePassword(Account account, String newPassword);

    boolean checkAnswer(Account account, String answer);

    boolean checkPassword(Account account, String oldPassword);

    List<Account> listAccount();

    void BlockUser(String userName);

    void DeleteUser(String userName);

    void UpdateAccountProfile(long AccountID, String name, String surname, int Age, String hobby);

    Account getAccountID(String userName);

    UserProfile getUserProfile(String userName);
}
