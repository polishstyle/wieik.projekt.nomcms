package wieik.nom.dao;

import wieik.isi.nom.dao.AuditableDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Article;
import wieik.nom.dto.Section;
import wieik.nom.vo.LinkVO;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface ArticleDao extends AuditableDao<Article> {
    List<Article> getNewest(Section section, int limit);

    List<Article> getForSection(Section section, int page, int limit);

    List<Article> getNewestWithPriority(int priority, int limit);

    List<Article> getInEdition(Section section, int page, int limit);

    List<Article> getInCheck(Section section, int page, int limit);

    Article findByName(String articleName);

    boolean wasVoting(Account account, Article article);

    void increaseVotesYes(long id, int i);

    void increaseVotesNo(long id, int i);

    List<LinkVO> getTopLinks(int n);

    void setAcceptArticleInCheck(String ArticleName);

    void setArticlePriority(String ArticleName, String newPriority);

    void deleteArticle(String ArticleName);
}
