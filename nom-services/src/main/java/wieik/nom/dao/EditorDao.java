package wieik.nom.dao;

import wieik.isi.nom.dao.Dao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Editor;
import wieik.nom.dto.Section;

/**
 * Created by Sebastian on 2015-05-02.
 */
public interface EditorDao extends Dao<Editor> {
    Editor findByAccount(Account account);

    boolean isInChief(Section section, Editor editor);
}
