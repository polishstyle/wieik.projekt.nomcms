package wieik.nom.exceptions;

/**
 * Created by Sebastian on 2015-03-12.
 */
public class NOMException extends Exception {
    public NOMException() {
        super();
    }

    public NOMException(String message) {
        super(message);
    }

    public NOMException(String message, Throwable cause) {
        super(message, cause);
    }

    public NOMException(Throwable cause) {
        super(cause);
    }

    protected NOMException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static String generateMessage(Exception e) {
        if (e.getMessage() != null)
            return e.getMessage();
        return e.toString();
    }
}
