package wieik.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.AuditableEntity;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Sebastian on 2015-05-02.
 */
// Nie dodajemy listy komentarzy - bedaone pobierane osobno
@Entity
@Table(name = "Articles")
@AttributeOverride(name = "id", column = @Column(name = "ArticleID"))
@NamedQueries({
        @NamedQuery(name = "getNewestArticlesForSection",
                query = "select a from Article a where a.section.id = :sectionId and a.status = A and a.state = P order by a.id desc"),
        @NamedQuery(name = "getArticlesForSection",
                query = "select a from Article a where a.section.id = :sectionId and a.status = 'A' and a.state = 'P' order by a.id desc"),
        @NamedQuery(name = "getArticlesInEditionForSection",
                query = "select a from Article a where a.section.id = :sectionId and a.status = A and a.state = E order by a.id desc"),
        @NamedQuery(name = "getArticlesInCheckForSection",
                query = "select a from Article a where a.section.id = :sectionId and a.status = 'A' and a.state = 'C' order by a.id desc"),
        @NamedQuery(name = "getNewestArticlesWithPriority",
                query = "select a from Article a where a.priority = :priority and a.status = A and a.state = P order by a.id desc"),
        @NamedQuery(name = "findArticleByName",
                query = "select a from Article a where a.name = :articleName"),
        @NamedQuery(name = "countArticlesForArticle",
                query = "select count(a) from Article a where a.section.id = :sectionId")
})
public class Article extends AuditableEntity {
    public final static int TITLE_MIN_SIZE = 16;
    public final static int TITLE_MAX_SIZE = 32;
    public final static int PRIORITY_MIN = 1;
    public final static int PRIORITY_MAX = 4;
    public final static char STATE_PUBLICATED = 'P';
    public final static char STATE_EDITION = 'E';
    public final static char STATE_CHECKING = 'C';

    @NotEmpty
    @Size(min = TITLE_MIN_SIZE, max = TITLE_MAX_SIZE)
    @Column(name = "Title")
    private String title;

    @NotEmpty
    @Size(min = 3, max = TITLE_MAX_SIZE)
    @Column(name = "Name")
    private String name;

    @NotEmpty
    @Column(name = "Content")
    private String content;

    @NotEmpty
    @Column(name = "Preview")
    private String preview;

    @Column(name = "ImageName")
    private String imageName;

    @Min(value = PRIORITY_MIN)
    @Max(value = PRIORITY_MAX)
    @Column(name = "Priority")
    private int priority;

    @Column(name = "PublicationDate")
    private Date publicationDate;

    @Column(name = "State")
    private char state;

    @ManyToOne
    @JoinColumn(name = "SectionID")
    private Section section;

    @ManyToOne
    @JoinColumn(name = "EditorID")
    private Editor author;

    @ManyToOne
    @JoinColumn(name = "TemplateID")
    private Theme theme;

    // licznik glosow aktualizowany wyzwalaczem, zeby nie trzeba bylo co chwile liczyc glosow w BD
    @Min(value = 0)
    @Column(name = "VotesYes")
    private int votesYes;

    @Min(value = 0)
    @Column(name = "VotesNo")
    private int votesNo;

    public Article() {
    }

    public Article(String title, String name, String content, String preview, String imageName, Section section, Editor author, Theme theme) {
        this.title = title;
        this.name = name;
        this.content = content;
        this.preview = preview;
        this.imageName = imageName;
        this.section = section;
        this.author = author;
        if (theme==null) {
            theme = section.getDefaultTheme();
        }
        this.theme = theme;
        //this.publicationDate = null;
        this.publicationDate = new Date();
        //this.state='E';
        this.state='P';
        this.votesYes = 0;
        this.votesNo = 0;
        this.priority = 1;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public boolean isPublicated() {
        return state == STATE_PUBLICATED;
    }

    public void publicate() {
        state = STATE_PUBLICATED;
    }

    public boolean isInCheck() {
        return state == STATE_CHECKING;
    }

    public void toCheck() {
        state = STATE_CHECKING;
    }

    public boolean isInEdition() {
        return state == STATE_EDITION;
    }

    public void toEdition() {
        state = STATE_EDITION;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public char getState() {
        return state;
    }

    public void setState(char state) {
        this.state = state;
    }

    public int getVotesYes() {
        return votesYes;
    }

    public void setVotesYes(int votesYes) {
        this.votesYes = votesYes;
    }

    public int getVotesNo() {
        return votesNo;
    }

    public void setVotesNo(int votesNo) {
        this.votesNo = votesNo;
    }

    public Editor getAuthor() {
        return author;
    }

    public void setAuthor(Editor author) {
        this.author = author;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public void increaseVotesYes() {
        ++votesYes;
    }

    public void increaseVotesNo() {
        ++votesNo;
    }
}
