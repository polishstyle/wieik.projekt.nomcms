package wieik.nom.dto;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

/**
 * Created by Sebastian on 2015-03-16.
 */
@Entity
@Table(name = "Accounts")
@AttributeOverride(name = "id", column = @Column(name = "AccountID"))
@NamedQueries({
        @NamedQuery(
                name = "findAccountByUsername",
                query = "select a from Account a where a.username = :username"),
        @NamedQuery(
                name = "findAccountByEmail",
                query = "select a from Account a where a.email = :email"),
        @NamedQuery(
                name = "getActivationCodeSentDate",
                query = "select a.activationCodeSentDate from Account a where a.id = :id")
})
public class Account extends BaseEntity {
    public static final String PERMISSION_PREFIX = "ROLE_";
    public static final String SEX_REGEXP = "^[MF]$";
    public static final String SEX_MALE = "M";
    public static final String SEX_FEMALE = "F";
    public static final int USERNAME_MIN_SIZE = 6;
    public static final int USERNAME_MAX_SIZE = 32;
    public static final int EMAIL_MIN_SIZE = 6;
    public static final int EMAIL_MAX_SIZE = 64;
    public static final int PASSWORD_MIN_SIZE = 6;
    public static final int PASSWORD_MAX_SIZE = 64;
    public static final int QUESTION_MAX_SIZE = 32;


    @NotEmpty
    @Size(min = USERNAME_MIN_SIZE, max = USERNAME_MAX_SIZE)
    private String username;

    @NotEmpty
    @Email
    @Column(name = "Email")
    private String email;

    @Column(name = "Enabled")
    private boolean enabled;
    
    @NotNull
    @Pattern(regexp = SEX_REGEXP, message = "validation.wrong.sex")
    @Column(name = "Sex")
    private String sex;

    @Column(name = "Created")
    private Date created;

    @Column(name = "ActivationCodeSent", nullable = false)
    private Date activationCodeSentDate;

    @NotEmpty
    @Size(max = 254)
    @Column(name = "Question", nullable = false)
    private String question;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "AccountRole",
            joinColumns = {
                    @JoinColumn(name = "AccountID", nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "RoleID", nullable = false, updatable = false)})
    private Set<Role> roles;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Date getActivationCodeSentDate() {
        return activationCodeSentDate;
    }

    public void setActivationCodeSentDate(Date activationCodeSentDate) {
        this.activationCodeSentDate = activationCodeSentDate;
    }


}
