package wieik.nom.dto;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
@Entity
@Table(name = "Editors")
@AttributeOverride(name = "id", column = @Column(name = "EditorID"))
@NamedQueries({
        @NamedQuery(name = "findEditorByAccount",
                query = "select a from Editor a where a.account.id = :accountId")
})
public class Editor extends Employee {
    // w ktorych sekcjaj jest redaktorem
    @ManyToMany
    @JoinTable(name = "SectionEditor",
            inverseJoinColumns = {
                    @JoinColumn(name = "SectionID", nullable = false, updatable = false)},
            joinColumns = {
                    @JoinColumn(name = "EditorID", nullable = false, updatable = false)})
    private List<Section> sections;
    // w ktorych sekcjach jest naczelnym
    @ManyToMany
    @JoinTable(name = "SectionInChief",
            inverseJoinColumns = {
                    @JoinColumn(name = "SectionID", nullable = false, updatable = false)},
            joinColumns = {
                    @JoinColumn(name = "EditorID", nullable = false, updatable = false)})
    private List<Section> inChief;
    // w ktorych jest modeteratorem
    @ManyToMany
    @JoinTable(name = "SectionModerator",
            inverseJoinColumns = {
                    @JoinColumn(name = "SectionID", nullable = false, updatable = false)},
            joinColumns = {
                    @JoinColumn(name = "EditorID", nullable = false, updatable = false)})
    private List<Section> moderator;

    public List<Section> getModerator() {
        return moderator;
    }

    public void setModerator(List<Section> moderator) {
        this.moderator = moderator;
    }

    public List<Section> getInChief() {
        return inChief;
    }

    public void setInChief(List<Section> inChief) {
        this.inChief = inChief;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Editor)) return false;

        Editor editor = (Editor) o;

        return id==editor.id;

    }

    @Override
    public int hashCode() {
        return new Long(id).hashCode();
    }
}
