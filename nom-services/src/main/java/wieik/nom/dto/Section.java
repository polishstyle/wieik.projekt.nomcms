package wieik.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Created by Sebastian on 2015-05-02.
 */
// nie umieszczamy listy artykulow, bo nie bedziemy je osobno pobierac w okreslonej ilosci
@Entity
@Table(name = "Sections")
@AttributeOverride(name = "id", column = @Column(name = "SectionID"))
@NamedQueries({
        @NamedQuery(name = "findSectionByName", query = "select s from Section s where s.name = :sectionName")
})
public class Section extends BaseEntity {
    public final static String NAME_REGEXP = "[a-zA-Z]+";
    @NotEmpty
    @Pattern(regexp = NAME_REGEXP, message = "validation.wrong.section.name")
    @Column(name = "Name")
    private String name;
    @Column(name = "Priority")
    private int priority;
    @ManyToMany
    @JoinTable(name = "SectionInChief",
            joinColumns = {
                    @JoinColumn(name = "SectionID", nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "EditorID", nullable = false, updatable = false)})
    private List<Editor> editorsInChief;
    @ManyToOne
    @JoinColumn(name = "TemplateID")
    private Theme defaultTheme;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<Editor> getEditorsInChief() {
        return editorsInChief;
    }

    public void setEditorsInChief(List<Editor> editorsInChief) {
        this.editorsInChief = editorsInChief;
    }

    public Theme getDefaultTheme() {
        return defaultTheme;
    }

    public void setDefaultTheme(Theme defaultTheme) {
        this.defaultTheme = defaultTheme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Section)) return false;

        Section section = (Section) o;

        return name.equals(section.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
