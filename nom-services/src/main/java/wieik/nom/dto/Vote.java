package wieik.nom.dto;

import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.*;

/**
 * Created by Sebastian on 2015-05-02.
 */
@Entity
@Table(name = "Votes")
@AttributeOverride(name = "id", column = @Column(name = "VoteID"))
@NamedQueries({
        @NamedQuery(name = "findVote", query = "select v from Vote v where v.voter.id = :voterId and v.article.id = :articleId")
})
public class Vote extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "AccountID")
    private Account voter;
    @OneToOne
    @JoinColumn(name = "ArticleID")
    private Article article;
    @Column(name = "Ok")
    private boolean ok;

    public Account getVoter() {
        return voter;
    }

    public void setVoter(Account voter) {
        this.voter = voter;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }
}
