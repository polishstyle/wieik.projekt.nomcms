package wieik.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Sebastian on 2015-05-02.
 */
@Entity
@Table(name = "Comments")
@AttributeOverride(name = "id", column = @Column(name = "CommentID"))
@NamedQueries({
        @NamedQuery(name = "getCommentsForArticle",
                query = "select a from Comment a where a.articleId = :articleId order by a.id desc"),
        @NamedQuery(name = "countCommentsForArticle",
                query = "select count(c) from Comment c where c.articleId = :articleId")
})
public class Comment extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "AccountID")
    private Account user;
    @NotEmpty
    @Column(name = "Content")
    private String content;

    @NotNull
    @Column(name = "CreationDate")
    private Date createDate;

    @NotNull
    @Column(name = "ArticleID")
    private long articleId;

    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(long articleId) {
        this.articleId = articleId;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
