package wieik.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.*;

/**
 * Created by Sebastian on 2015-05-02.
 */
@Entity
@Table(name = "Templates")
@AttributeOverride(name = "id", column = @Column(name = "TemplateID"))
@NamedQueries({
        @NamedQuery(name = "findThemeByName", query = "select t from Theme t where t.name = :themeName")
})
public class Theme extends BaseEntity {
    @NotEmpty
    @Column(name = "Name")
    private String name;
    @NotEmpty
    @Column(name = "Body")
    private String body;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
