package wieik.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-03-16.
 */
@Entity
@Table(name = "Roles")
@AttributeOverride(name = "id", column = @Column(name = "RoleID"))
@NamedQueries({
        @NamedQuery(
                name = "findRoleByName",
                query = "select r from Role r where r.name = :name"),
})
public class Role extends BaseEntity {
    public static final String ROLE_USER = "USER";
    public static final int NAME_MIN_SIZE = 3;
    public static final int NAME_MAX_SIZE = 16;
    public static final int DESC_MIN_SIZE = 1;
    public static final int DESC_MAX_SIZE = 255;

    @NotEmpty
    @Size(min = NAME_MIN_SIZE, max = NAME_MAX_SIZE)
    @Column(name = "Name")
    private String name;

    @NotEmpty
    @Size(min = DESC_MIN_SIZE, max = DESC_MAX_SIZE)
    @Column(name = "Description")
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (!name.equals(role.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
