package wieik.nom.dto;

/**
 * Created by Andrzej on 2015-06-24.
 */
public class UserProfile {

    private String Name;
    private String Surname;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getHobby() {
        return Hobby;
    }

    public void setHobby(String hobby) {
        Hobby = hobby;
    }

    private int Age;
    private String Hobby;
}
