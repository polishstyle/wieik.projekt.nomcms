package wieik.nom.dto;

import org.hibernate.validator.constraints.NotEmpty;
import wieik.isi.nom.dto.BaseEntity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

/**
 * Created by Sebastian on 2015-05-02.
 */
@MappedSuperclass
public abstract class Employee extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "AccountID")
    private Account account;
    @NotEmpty
    @Column(name = "FirstName")
    private String firstName;
    @NotEmpty
    @Column(name = "LastName")
    private String lastName;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
