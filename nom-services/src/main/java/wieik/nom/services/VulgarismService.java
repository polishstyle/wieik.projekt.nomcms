package wieik.nom.services;

/**
 * Created by Sebastian on 2015-05-04.
 */
public interface VulgarismService {
    void add(String vulgarism);

    void delete(String vulgarism);

    boolean isVulgarism(String word);

    String censors(String text);

    boolean check(String text);
}
