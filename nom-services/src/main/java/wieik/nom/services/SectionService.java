package wieik.nom.services;

import wieik.nom.dto.Editor;
import wieik.nom.dto.Section;
import wieik.nom.exceptions.NOMException;
import wieik.nom.vo.LinkVO;
import wieik.nom.vo.SectionVO;

import java.util.List;

/**
 * Created by Sebastian on 2015-06-05.
 */
public interface SectionService {
    Section getSectionByName(String sectionName) throws NOMException;

    List<LinkVO> getLinksWithPriority(Section section, int priority, int n);

    List<LinkVO> getLinks(Section section, int currentPage, int limit);

    int coutArticles(Section section);

    List<String> getSectionNames();

    List<SectionVO> getSectionVOsWithTopLinks(int n);

    List<SectionVO> getSectionVOs(int n);

    boolean canCreate(Section section, Editor editor);
}
