package wieik.nom.services;

import wieik.nom.dto.Theme;
import wieik.nom.exceptions.NOMException;

/**
 * Created by Sebastian on 2015-06-08.
 */
public interface ThemeService {
    Theme getByName(String name);

    Theme getDefault() throws NOMException;

}
