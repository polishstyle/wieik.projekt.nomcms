package wieik.nom.services;

import org.springframework.validation.Errors;
import wieik.nom.dto.Account;
import wieik.nom.exceptions.NOMException;

import java.util.List;

/**
 * Created by Sebastian on 2015-03-12.
 */
public interface AccountService {
    boolean registerUserAccount(Account account, String password, String answer, Errors errors) throws NOMException;

    String getEncodedPassword(Account account) throws NOMException;

    Account findByUsername(String email) throws NOMException;

    void activateAccount(long id, String d) throws NOMException;

    void activateAccount(String email, String d) throws NOMException;

    void resetPassword(long id, String answer, Errors errors) throws NOMException;

    void sendActivationEmail(String email) throws NOMException;

    Account findByEmail(String email) throws NOMException;

    boolean checkPassword(Account account, String oldPassword) throws NOMException;

    void changePassword(Account account, String newPassword) throws NOMException;

    List<Account> listAccount();
}
