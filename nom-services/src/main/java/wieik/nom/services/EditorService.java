package wieik.nom.services;

import wieik.nom.dto.Account;
import wieik.nom.dto.Editor;

/**
 * Created by Sebastian on 2015-06-10.
 */
public interface EditorService {
    Editor getEditor(Account account);
}
