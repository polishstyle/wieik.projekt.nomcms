package wieik.nom.services;

import wieik.nom.dto.Article;
import wieik.nom.dto.Comment;
import wieik.nom.dto.Section;
import wieik.nom.dto.Editor;
import wieik.nom.vo.LinkVO;

import java.util.List;

/**
 * Created by Sebastian on 2015-06-05.
 */
public interface ArticleService {
    Article findArticle(String articleName);

    /**
     * Proba zaglosowania na artykul przez uzytkownika.
     *
     * @param articleName nazwa artykulu
     * @param vote        rodzaj glosu: 1-tak, 0-nie
     * @param username    nazwa uzytkownika glosujacego
     */
    void tryToVote(String articleName, int vote, String username);

    List<Comment> getComments(Article article, int page, int limit);

    void addComment(String articleName, String comment, String username);

    int commentsPages(Article article, int limit);

    List<LinkVO> getTopLinks(int n);

    void update(Article article, String username);

    void create(Article article);

    boolean canEdit(Article article, Editor editor);
}
