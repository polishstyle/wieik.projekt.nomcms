package wieik.nom.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.nom.dto.*;
import wieik.nom.services.ArticleService;
import wieik.nom.vo.LinkVO;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2015-06-05.
 */
@Service
@Transactional(readOnly = true)
public class ArticleServiceImpl extends BaseServiceImpl implements ArticleService {

    public Article findArticle(String articleName) {
        Article article = null;
        try {
            article = articleDao.findByName(articleName);
        } catch (Exception e) {
        }
        return article;
    }

    @Transactional(readOnly = false)
    public void tryToVote(String articleName, int vote, String username) {
        try {
            Article article = findArticle(articleName);
            if (article != null) {
                Account account = accountDao.findByUsername(username);
                if (account != null) {
                    if (!articleDao.wasVoting(account, article)) {
                        Vote v = new Vote();
                        v.setArticle(article);
                        v.setVoter(account);
                        if (vote == 1) {
                            v.setOk(true);
                            articleDao.increaseVotesYes(article.getId(), article.getVotesYes() + 1);
                        } else {
                            v.setOk(false);
                            articleDao.increaseVotesNo(article.getId(), article.getVotesNo() + 1);
                        }
                        voteDao.createVote(article.getId(), v.isOk() ? 1 : 0, account.getId());
                    } else {
                    }
                }
            }
        } catch (Exception e) {
            System.err.println(e.toString());
            e.printStackTrace();
        }
    }

    public List<Comment> getComments(Article article, int page, int limit) {
        try {
            List<Comment> c = commentDao.getForArticle(article, page, limit);
            return c;
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        }
    }

    @Transactional(readOnly = false)
    public void addComment(String articleName, String comment, String username) {
        Article article = articleDao.findByName(articleName);
        if (article != null) {
            Account account = accountDao.findByUsername(username);
            if (account != null) {
                Comment c = new Comment();
                c.setArticleId(article.getId());
                c.setContent(comment);
                c.setUser(account);
                c.setCreateDate(new Date());
                System.err.println("!!!! dodaje komentarz");
                commentDao.create(c);
            }
        }
    }

    public int commentsPages(Article article, int limit) {
        int total = commentDao.countForArticle(article.getId());
        return getNumberOfPages(total, limit);
    }

    public List<LinkVO> getTopLinks(int n) {
        try {
            List<LinkVO> links = articleDao.getTopLinks(n);
            for (LinkVO e : links) {
                e.setLink("/NOM/p/" + sectionDao.get(e.getSectionID()).getName() + '/' + e.getLink());
            }
            return links;
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    @Transactional(readOnly = false)
    public void create(Article article) {
        articleDao.create(article, article.getAuthor().getAccount().getUsername());
    }

    public boolean canEdit(Article article, Editor editor) {
        if (article.getAuthor().equals(editor)) {
            return true;
        }
        // czy naczelny
        return editorDao.isInChief(article.getSection(), editor);
    }

    @Transactional(readOnly = false)
    public void update(Article article, String username) {
        articleDao.update(article, username);
    }
}
