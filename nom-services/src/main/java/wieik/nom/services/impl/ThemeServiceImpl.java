package wieik.nom.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.nom.dto.Theme;
import wieik.nom.exceptions.NOMException;
import wieik.nom.services.ThemeService;

/**
 * Created by Sebastian on 2015-06-08.
 */
@Service
@Transactional(readOnly = true)
public class ThemeServiceImpl extends BaseServiceImpl implements ThemeService {

    public Theme getByName(String name) {
        try {
            return themeDao.getByName(name);
        } catch (Exception e) {
            return null;
        }
    }

    public Theme getDefault() throws NOMException {
        try {
            return themeDao.getDefault();
        } catch (Exception e) {
            throw new NOMException("Cannot finddefault theme.", e);
        }
    }
}
