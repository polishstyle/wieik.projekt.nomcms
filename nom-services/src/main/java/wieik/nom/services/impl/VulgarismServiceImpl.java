package wieik.nom.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.nom.services.VulgarismService;

import java.util.List;

/**
 * Created by Sebastian on 2015-05-04.
 */
@Service
@Transactional(readOnly = true)
public class VulgarismServiceImpl extends BaseServiceImpl implements VulgarismService {
    private List<String> cache = null;

    public void init() {
        cache = vulgarismDao.getAll();
    }

    @Transactional(readOnly = false)
    public void add(String vulgarism) {
        vulgarismDao.add(vulgarism.toLowerCase());
        cache = vulgarismDao.getAll();
    }

    @Transactional(readOnly = false)
    public void delete(String vulgarism) {
        vulgarismDao.delete(vulgarism.toLowerCase());
        cache = vulgarismDao.getAll();
    }

    public boolean isVulgarism(String word) {
        if (cache == null)
            cache = vulgarismDao.getAll();
        return cache.contains(word.toLowerCase());
    }

    public String censors(String text) {
        String[] words = text.split("\\s+");
        for (String word : words) {
            if (isVulgarism(word)) {
                text = text.replaceAll(word, "*****");
            }
        }
        return text;
    }

    public boolean check(String text) {
        String[] words = text.split("\\s+");
        for (String word : words) {
            if (isVulgarism(word))
                return false;
        }
        return true;
    }
}
