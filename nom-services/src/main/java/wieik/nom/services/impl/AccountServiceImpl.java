package wieik.nom.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.validation.Errors;
import wieik.nom.dao.hibernate.AccountHbnDao;
import wieik.nom.dto.Account;
import wieik.nom.dto.Role;
import wieik.nom.exceptions.NOMException;
import wieik.nom.mail.ActivationMailData;
import wieik.nom.mail.NomMailSender;
import wieik.nom.services.AccountService;
import wieik.nom.utils.RandomString;

import javax.inject.Inject;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Sebastian on 2015-03-12.
 */
@Service
@Transactional(readOnly = true)
public class AccountServiceImpl extends BaseServiceImpl implements AccountService {


    @Inject
    NomMailSender nomMailSender;

    @Inject
    private AccountHbnDao accountHbnDao;

    @Value("#{accountServiceProps.confirmationKey}")
    private String confirmationKey;

    @Value("#{accountServiceProps.activationExpiresInMS}")
    private long activationExpiresInMS;

    @Value("#{accountServiceProps.emailActivation}")
    private boolean emailActivation;


    @Transactional(readOnly = false)
    public boolean registerUserAccount(Account account, String password, String answer, Errors errors) throws NOMException {
        try {
            validateUsername(account.getUsername(), errors);
            validateEmail(account.getEmail(), errors);
            boolean valid = !errors.hasErrors();
            if (valid) {
                if (emailActivation) {
                    registerUserAccountAndSendActivationEmail(account, password, answer);
                } else {
                    registerUserAccountAndActivate(account, password, answer);
                }
            }
            return valid;
        } catch (Exception e) {
            error("Error registering account: " + e.toString(), e);
            throw new NOMException("Error registering account", e);
        }
    }

    private void validateUsername(String username, Errors errors) {
        if (accountDao.findByUsername(username) != null) {
            errors.rejectValue("username", "error.duplicate.username", new String[]{username}, null);
        }
    }

    private void validateEmail(String email, Errors errors) {
        if (accountDao.findByEmail(email) != null) {
            errors.rejectValue("email", "error.duplicate.email", new String[]{email}, null);
        }
    }

    private void registerUserAccountAndSendActivationEmail(Account account, String password, String answer) {
        account.setRoles(getRolesForUser());
        account.setEnabled(false);
        Date sentActivationCode = new Date();
        account.setActivationCodeSentDate(sentActivationCode);
        long id = accountDao.create(account, password, answer);
        Date activationCodeSentDate = accountDao.getActivationCodeSentDate(id);
        // Baza danych zaokrągla milisekundy, potrzebujemy takiej daty, jaka jest w bazie
        String activationCode = getActivationCode(id, activationCodeSentDate.getTime());
        ActivationMailData activationMailData = new ActivationMailData(id, account.getEmail(), activationCode, sentActivationCode);
        nomMailSender.sendActivationEmail(activationMailData);
    }

    private void registerUserAccountAndActivate(Account account, String password, String answer) {
        account.setRoles(getRolesForUser());
        account.setEnabled(true);
        Date sentActivationCode = new Date();
        account.setActivationCodeSentDate(sentActivationCode);
        accountDao.create(account, password, answer);
    }

    private Set<Role> getRolesForUser() {
        Role roleUser = roleDao.getByName(Role.ROLE_USER);
        Set<Role> roles = new HashSet<Role>();
        roles.add(roleUser);
        return roles;
    }

    private String getActivationCode(long id, long activationCodeSentDateInMS) {
        String digest = DigestUtils.md5DigestAsHex((id + ":" + activationCodeSentDateInMS + ":" + confirmationKey).getBytes());
        return digest;
    }

    public String getEncodedPassword(Account account) throws NOMException {
        try {
            return accountDao.getEncodedPassword(account);
        } catch (Exception e) {
            error("Error getting encoded password: " + e.toString(), e);
            throw new NOMException("Error registering account", e);
        }
    }

    public Account findByUsername(String username) throws NOMException {
        try {
            return accountDao.findByUsername(username);
        } catch (Exception e) {
            error("Error finding account with username " + username + ": " + e.toString(), e);
            throw new NOMException("Error registering account", e);
        }
    }

    @Transactional(readOnly = false)
    public void activateAccount(long id, String digest) throws NOMException {
        Account account = accountDao.get(id);
        activateAccount(account, digest);
    }

    private void activateAccount(Account account, String digest) throws NOMException {
        if (account == null)
            throw new NOMException();
        if (isActivationCodeExpired(account.getActivationCodeSentDate()))
            throw new NOMException();
        if (!isActivationCodeCorrect(digest, account)) {
            throw new NOMException();
        }
        account.setEnabled(true);
        accountDao.update(account);
    }

    private boolean isActivationCodeExpired(Date activationCodeSentDate) {
        long now = (new Date()).getTime();
        long sentActivationCode = activationCodeSentDate.getTime();
        if (sentActivationCode + activationExpiresInMS < now) {
            return true;
        }
        return false;
    }

    private boolean isActivationCodeCorrect(String digest, Account account) {
        String correctDigest = getActivationCode(account.getId(), account.getActivationCodeSentDate().getTime());
        if (digest.equals(correctDigest)) {
            return true;
        }
        return false;
    }

    @Transactional(readOnly = false)
    public void activateAccount(String email, String digest) throws NOMException {
        Account account = accountDao.findByEmail(email);
        activateAccount(account, digest);
    }

    @Transactional(readOnly = false)
    public void resetPassword(long id, String answer, Errors errors) throws NOMException {
        try {
            Account account = accountDao.get(id);
            if (account == null) {
                throw new NOMException("Account not found.");
            }
            if (accountDao.checkAnswer(account, answer)) {
                System.err.println("ODPOWIEDZ DOBRA");
                String newPassword = RandomString.randomString(12);
                accountDao.changePassword(account, newPassword);
                nomMailSender.sendNewPassword(account.getEmail(), newPassword);
            } else {
                errors.rejectValue("answer", "error.answer", new String[]{answer}, null);
                System.err.println("ZLA ODPOWIEDZ");
            }


        } catch (Exception e) {
            System.err.println("Jakis inny blad");
            e.printStackTrace();
            throw new NOMException(e);
        }
    }

    @Transactional(readOnly = false)
    public void sendActivationEmail(String email) throws NOMException {
        Account account = accountDao.findByEmail(email);
        if (account != null) {
            Date activationCodeSentDate = new Date();
            account.setActivationCodeSentDate(activationCodeSentDate);
            accountDao.update(account);
            activationCodeSentDate = accountDao.getActivationCodeSentDate(account.getId());
            String activationCode = getActivationCode(account.getId(), activationCodeSentDate.getTime());
            ActivationMailData activationMailData = new ActivationMailData(account.getId(), account.getEmail(), activationCode, activationCodeSentDate);
            nomMailSender.sendActivationEmail(activationMailData);
        }
    }

    public Account findByEmail(String email) throws NOMException {
        try {
            return accountDao.findByEmail(email);
        } catch (Exception e) {
            error("Error finding account with email " + email + ": " + e.toString(), e);
            throw new NOMException("Error registering account", e);
        }
    }

    public boolean checkPassword(Account account, String oldPassword) throws NOMException {
        try {
            return accountDao.checkPassword(account, oldPassword);
        } catch (Exception e) {
            throw new NOMException(e);
        }
    }

    @Transactional(readOnly = false)
    public void changePassword(Account account, String newPassword) throws NOMException {
        try {
            accountDao.changePassword(account, newPassword);
        } catch (Exception e) {
            error("Error changing password", e);
            throw new NOMException(e);
        }
    }

    @Transactional
    public List<Account> listAccount()
    {
        return accountHbnDao.listAccount();
    }
}
