package wieik.nom.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.nom.dto.Account;
import wieik.nom.dto.Editor;
import wieik.nom.services.EditorService;

/**
 * Created by Sebastian on 2015-06-10.
 */
@Service
@Transactional(readOnly = true)
public class EditorServiceImpl extends BaseServiceImpl implements EditorService {
    public Editor getEditor(Account account) {
        try {
            return editorDao.findByAccount(account);
        } catch (Exception e) {
            return null;
        }
    }
}
