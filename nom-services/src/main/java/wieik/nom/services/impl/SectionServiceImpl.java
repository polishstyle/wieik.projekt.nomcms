package wieik.nom.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.nom.dto.Editor;
import wieik.nom.dto.Section;
import wieik.nom.exceptions.NOMException;
import wieik.nom.services.SectionService;
import wieik.nom.vo.LinkVO;
import wieik.nom.vo.SectionVO;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sebastian on 2015-06-05.
 */
@Service
@Transactional(readOnly = true)
public class SectionServiceImpl extends BaseServiceImpl implements SectionService {
    public Section getSectionByName(String sectionName) throws NOMException {
        try {
            return sectionDao.findByName(sectionName);
        } catch (Exception e) {
            return null;
        }
    }

    public List<LinkVO> getLinksWithPriority(Section section, int priority, int n) {
        try {
            return sectionDao.getLinksWithPriority(section, priority, n);
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public List<LinkVO> getLinks(Section section, int page, int limit) {
        try {
            return sectionDao.getLinks(section, page, limit);
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public int coutArticles(Section section) {
        try {
            return sectionDao.coutArticles(section);
        } catch (Exception e) {
            return 0;
        }
    }

    public List<String> getSectionNames() {
        List<Section> sections = sectionDao.getAll();
        sections.sort(new Comparator<Section>() {
            public int compare(Section o1, Section o2) {
                return Integer.compare(o2.getPriority(), o1.getPriority());
            }
        });
        List<String> names = new LinkedList<String>();
        for (Section s : sections) {
            names.add(s.getName());
        }
        return names;
    }

    public List<SectionVO> getSectionVOsWithTopLinks(int n) {
        try {
            List<SectionVO> sectionVOs = new LinkedList<SectionVO>();
            List<Section> sections = sectionDao.getAll();
            sections.sort(new Comparator<Section>() {
                public int compare(Section o1, Section o2) {
                    return Integer.compare(o2.getPriority(), o1.getPriority());
                }
            });

            for (Section s : sections) {
                List<LinkVO> links = sectionDao.getTopLinks(s, n);
                sectionVOs.add(new SectionVO(s.getName(), links));
            }
            return sectionVOs;
        } catch (Exception e) {
            return emptySectionVOs();
        }
    }

    private List<SectionVO> emptySectionVOs() {
        List<String> sectionNames = getSectionNames();
        List<SectionVO> sectionVOs = new LinkedList<SectionVO>();
        for (String s : sectionNames) {
            sectionVOs.add(new SectionVO(s, Collections.EMPTY_LIST));
        }
        return sectionVOs;
    }

    public List<SectionVO> getSectionVOs(int n) {
        try {
            List<SectionVO> sectionVOs = new LinkedList<SectionVO>();
            List<Section> sections = sectionDao.getAll();
            sections.sort(new Comparator<Section>() {
                public int compare(Section o1, Section o2) {
                    return Integer.compare(o2.getPriority(), o1.getPriority());
                }
            });

            for (Section s : sections) {
                List<LinkVO> links = sectionDao.getLinks(s, 0, n);
                sectionVOs.add(new SectionVO(s.getName(), links));
            }
            return sectionVOs;
        } catch (Exception e) {
            return emptySectionVOs();
        }
    }

    //TODO
    public boolean canCreate(Section section, Editor editor) {
        return true;
    }
}
