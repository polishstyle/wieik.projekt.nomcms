package wieik.nom.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wieik.nom.dao.*;

import javax.inject.Inject;

/**
 * Created by Sebastian on 2015-03-12.
 * Klasa bazowa dla uslug. Posiada fasade DAO.
 */
public abstract class BaseServiceImpl {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    protected void error(String msg, Exception e) {
        LOGGER.error(msg + " " + e.toString());
        e.printStackTrace();
    }

    @Inject
    protected AccountDao accountDao;

    @Inject
    protected RoleDao roleDao;

    @Inject
    protected ArticleDao articleDao;

    @Inject
    protected CommentDao commentDao;

    @Inject
    protected EditorDao editorDao;

    @Inject
    protected SectionDao sectionDao;

    @Inject
    protected ThemeDao themeDao;

    @Inject
    protected VoteDao voteDao;

    @Inject
    protected VulgarismDao vulgarismDao;


    protected int getNumberOfPages(int total, int max) {
        int pages = 0;
        if (total > max)
            pages = (total / max);
        if ((total % max) > 0)
            ++pages;
        return pages;
    }
}
