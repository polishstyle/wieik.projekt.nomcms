package wieik.nom.utils;

import java.util.Date;
import java.util.Random;

/**
 * Created by Sebastian on 2015-03-25.
 */
public class RandomString {
    private static final char[] alphaNumeric;
    private static final int alphaNumericLength;

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch)
            tmp.append(ch);
        for (char ch = 'a'; ch <= 'z'; ++ch)
            tmp.append(ch);
        for (char ch = 'A'; ch <= 'Z'; ++ch)
            tmp.append(ch);
        alphaNumeric = tmp.toString().toCharArray();
        alphaNumericLength = alphaNumeric.length;
    }

    public static String randomString(int length) {
        if (length < 1)
            throw new IllegalArgumentException("length < 1: " + length);
        Random random = new Random(new Date().getTime());
        StringBuilder s = new StringBuilder(length);
        int r;
        char c;
        for (int i = 0; i < length; ++i) {
            r = random.nextInt(alphaNumericLength);
            c = alphaNumeric[r];
            s.append(c);
        }
        return s.toString();
    }
}
