package wieik.nom.vo;

/**
 * Created by Sebastian on 2015-06-05.
 */
public class LinkVO {
    private String title;
    private String description;
    private String link;
    private int priority;
    private int sectionID;
    private String imageName;

    public LinkVO(String title, String description, String link, int priority, int sectionID, String imageName) {
        this.title = title;
        this.description = description;
        this.link = link;
        this.priority = priority;
        this.imageName = imageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
