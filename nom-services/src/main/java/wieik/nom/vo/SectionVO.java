package wieik.nom.vo;

import java.util.List;

/**
 * Created by Sebastian on 2015-06-09.
 */
public class SectionVO {
    private String name;
    private List<LinkVO> links;

    public SectionVO(String name, List<LinkVO> links) {
        this.name = name;
        this.links = links;
    }

    public String getName() {
        return name;
    }

    public List<LinkVO> getLinks() {
        return links;
    }
}
